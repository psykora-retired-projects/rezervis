/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package biz.sykora.rezervis.ejb;

import biz.sykora.rezervis.cdi.IdentityManager;
import biz.sykora.rezervis.model.Role;
import biz.sykora.rezervis.model.User;
import java.util.Arrays;
import java.util.logging.Level;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mockito;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class UserServiceTest {

    UserService us;
    
    @BeforeClass
    public static void initClass() {
        Logger.getLogger(UserService.class.getName()).setLevel(Level.OFF);
    }
    
    @Before
    public void init() {
        this.us = new UserService();
        this.us.identityManager = mock(IdentityManager.class);
        this.us.activityLogger = mock(ActivityLogger.class);
        this.us.em = mock(EntityManager.class);
    }
    
     @Test
     public void saveNewUserTest() {
        String email = "user@abc.com";
        String password = "mypassword";
        User u = new User();
        u.setEmail(email);
        u.setRoles(Arrays.asList(Role.USER));
        u.setPassword(UserService.hashPassword(password));
        this.us.saveNewUser(email, password);
        ArgumentCaptor<User> ucap = ArgumentCaptor.forClass(User.class);
        verify(this.us.em).persist(ucap.capture());
        assertThat(ucap.getValue().getEmail(), is(email));
        assertThat(ucap.getValue().getPassword(), is(UserService.hashPassword(password)));
        assertThat(ucap.getValue().getRoles(), contains(Role.USER));
        assertThat(ucap.getValue().getRoles(), not(contains(Role.ADMIN)));
        assertThat(ucap.getValue().getRoles(), not(contains(Role.MANAGER)));
        assertThat(ucap.getValue().getRoles(), not(contains(Role.OPERATOR)));
     }
}
