/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package biz.sykora.rezervis.ejb;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.mail.Address;
 
@Stateless
public class MailService {
    private static final Logger logger = Logger.getLogger(MailService.class.getName());
    
    @Resource(name = "isEmailDisabled")
    private boolean isEmailDisabled;
 
    @Resource(name = "java:jboss/mail/gmail")
    private Session sessionGmail;
 
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void send(String addresses, String topic, String textMessage) {
 
        // do not send the real e-mail, if the mail service is disabled
        if (isEmailDisabled) {
            logger.log(Level.WARNING, "Email service disabled. No email sent to: {0}", addresses);
            logger.log(Level.FINE, "Email topic: {0}", topic);
            logger.log(Level.FINEST, "Email message: {0}", textMessage);
            return;
        } 
        
        try {
 
            Message message = new MimeMessage(sessionGmail);
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(addresses));
            message.setSubject(topic);
            message.setText(textMessage);
 
            String replyTo = System.getProperty("replyto.email.address", "");
            if (!replyTo.isEmpty()) {
                message.setReplyTo(new Address[]{new InternetAddress(replyTo)});
            }
             
            Transport.send(message);
            logger.log(Level.INFO, "Email sent to {0}", addresses);
            logger.log(Level.FINE, "Email topic: {0}", topic);
            logger.log(Level.FINEST, "Email message: {0}", textMessage);
            
        } catch (MessagingException e) { 
            logger.log(Level.SEVERE, "Cannot send email", e);
            // TODO: throw the exception up
        }
    }

    //@Resource(name = "java:jboss/mail/sendgrid")
    @Resource(name = "java:jboss/mail/mailgun")
    private Session sendGridSession;
 
    // TODO: rename
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void send2(String addresses, String topic, String textMessage) {
 
       // do not send the real e-mail, if the mail service is disabled
        if (isEmailDisabled) {
            logger.log(Level.WARNING, "Email service disabled. No email sent to: {0}", addresses);
            logger.log(Level.FINE, "Email topic: {0}", topic);
            logger.log(Level.FINEST, "Email message: {0}", textMessage);
            return;
        } 
        
        try {
 
            Message message = new MimeMessage(sendGridSession);
            message.setFrom(new InternetAddress("rezervace@brezanek.cz"));  // TODO: it should be configurable
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(addresses));
            message.setSubject(topic);
            message.setText(textMessage);
             
            String replyTo = System.getProperty("replyto.email.address", "");
            if (!replyTo.isEmpty()) {
                message.setReplyTo(new Address[]{new InternetAddress(replyTo)});
            }
             
            Transport.send(message);
            logger.log(Level.INFO, "Email sent to {0}", addresses);
            logger.log(Level.FINE, "Email topic: {0}", topic);
            logger.log(Level.FINEST, "Email message: {0}", textMessage);
 
        } catch (MessagingException e) { 
            logger.log(Level.SEVERE, "Cannot send email", e);
             // TODO: throw the exception up
        }
    }
}