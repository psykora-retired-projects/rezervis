/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package biz.sykora.rezervis.ejb;

import biz.sykora.rezervis.model.CourseReservation;
import biz.sykora.rezervis.model.HistoryItem;
import biz.sykora.rezervis.model.Person;
import biz.sykora.rezervis.model.Season;
import java.util.Calendar;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class HistoryTool {

    @PersistenceContext(unitName = "rezervisdb")
    EntityManager em;

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void addToHistory(CourseReservation reservation, Season season) {
        Calendar c = Calendar.getInstance();
        Person person = reservation.getParticipant().getPerson();
        c.setTime(season.getDateBegin());
        long age = -1L;
        if (reservation.getParticipant().getDateBorn() != null) { // if participant's date born is valid
            age = (season.getDateBegin().getTime() - reservation.getParticipant().getDateBorn().getTime()) / (365L * 24L * 60L * 60L * 1000L);
        }
        HistoryItem hi = new HistoryItem(
                c.get(Calendar.YEAR),
                season.getName(),
                person.getUser().getEmail(),
                person.getName().getFullName(),
                reservation.getParticipant().getName().getFullName(),
                (int) age,
                reservation.getOfferedCourse().getName(),
                reservation.getOfferedCourse().getId()
        );
        this.em.persist(hi);
    }

}
