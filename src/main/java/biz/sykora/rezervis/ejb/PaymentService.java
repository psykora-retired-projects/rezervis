/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package biz.sykora.rezervis.ejb;

import biz.sykora.rezervis.cdi.IdentityManager;
import biz.sykora.rezervis.model.CourseReservation;
import biz.sykora.rezervis.model.Membership;
import biz.sykora.rezervis.model.Payment;
import biz.sykora.rezervis.model.PaymentType;
import biz.sykora.rezervis.model.Person;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class PaymentService {
    private static final Logger logger = Logger.getLogger(PaymentService.class.getName());

    @Inject
    private IdentityManager identityManager;
    
    @PersistenceContext(unitName = "rezervisdb")
    EntityManager em;

    public List<Payment> getPayments(){
        return this.em.createNamedQuery(Payment.findAllActive).getResultList();
    }
    
    public void createPayment(PaymentType paymentType, Date paymentDate, int amount, Person person, String comment, List<CourseReservation> reservations, List<String> membershipSeasons) {
        Person pers = this.em.find(Person.class, person.getId());
        Payment payment = new Payment();
        payment.setPaymentType(paymentType);
        payment.setPaymentDate(paymentDate);
        payment.setAmount(amount);
        payment.setPerson(pers);
        payment.setComment(comment);
        payment.setCreatedBy(identityManager.getUser().getEmail());
        this.em.persist(payment);
        logger.log(Level.INFO, "Payment has been created: {0}", payment);
        // process reservations
        reservations.forEach( (r) -> {
            Payment oldPaymnt = r.getPayment();
            if (oldPaymnt != null) {
                // the reservation has the payment already set, edit the comment
                payment.appendToComment("Nahrazuje platbu ID=", oldPaymnt.getId(), " pro rez.", r.getId(), ".");
                this.em.merge(payment);
                oldPaymnt.appendToComment("POZOR! Nahrazeno platbou ID=", payment.getId(), " pro rez.", r.getId(), ".");
                this.em.merge(oldPaymnt);
            }
            r.setPayment(payment);
            this.em.merge(r);
        });
        // process membership seasons and create related membership
        membershipSeasons.forEach( (s) -> {
            String[] ss = s.split("/");
            int year = Integer.valueOf(ss[0]);
            int halfYear = Integer.valueOf(ss[1]);
            Membership m = new Membership(pers, year, halfYear);
            m.setPayment(payment);
            m.setCreatedBy(identityManager.getUser().getEmail());
            pers.addMembership(m);
            this.em.persist(m);
            logger.log(Level.INFO, "Membership added for season {0}", s);
        });
        // TODO: activity log
    }
    
}
