/*
 * Copyright (c) 2014-2016, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package biz.sykora.rezervis.ejb;

import biz.sykora.rezervis.cdi.IdentityManager;
import biz.sykora.rezervis.jsf.util.StUtils;
import biz.sykora.rezervis.jsf.util.WeekDay;
import biz.sykora.rezervis.model.Audience;
import biz.sykora.rezervis.model.CourseReservation;
import biz.sykora.rezervis.model.Instructor;
import biz.sykora.rezervis.model.OfferedCourse;
import biz.sykora.rezervis.model.Participant;
import biz.sykora.rezervis.model.Season;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import static javax.ejb.TransactionAttributeType.SUPPORTS;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class CourseService {
    private static final Logger logger = Logger.getLogger(CourseService.class.getName());
    
    @Inject
    IdentityManager identityManager;
    
    @Inject
    MailService mailService;
    
    @Inject
    HistoryTool historyTool;
    
    @PersistenceContext(unitName = "rezervisdb")
    EntityManager em;

    public OfferedCourse findCourse(long id) {
        OfferedCourse course = this.em.find(OfferedCourse.class, id);
        Season season = course.getSeason();
        this.em.refresh(season);
        return course;
    }

    public CourseReservation findReservation(long id) {
        return this.em.find(CourseReservation.class, id);
    }
    
    @TransactionAttribute(SUPPORTS)
    public Season findCurrentSeason() {
        List<Season> seasons = this.em
                .createNamedQuery(Season.findCurrent, Season.class)
                .getResultList();
        return seasons.isEmpty() ? null : seasons.get(0);
        // TODO: what if seasons.size() > 1
    }    
    
    @TransactionAttribute(SUPPORTS)
    public Season findPreviousSeason() {
        List<Season> seasons = this.em
                .createNamedQuery(Season.findPrevious, Season.class)
                .getResultList();
        return seasons.isEmpty() ? null : seasons.get(0);
        // TODO: what if seasons.size() > 1
    }    
    
    @TransactionAttribute(SUPPORTS)
    public List<OfferedCourse> findExpiredCourses() {
        List<OfferedCourse> courses = this.em
                .createNamedQuery(OfferedCourse.findExpiredCourses, OfferedCourse.class)
                .getResultList();
        return courses;
    } 

    @TransactionAttribute(SUPPORTS)
    public List<OfferedCourse> findOfferedCourses(Audience audience) {
        List<OfferedCourse> courses = this.em
                .createNamedQuery(OfferedCourse.findOfferedByAudience, OfferedCourse.class)
                .setParameter("audience", audience)
                .getResultList();
        return courses;
    } 

    @TransactionAttribute(SUPPORTS)
    public List<OfferedCourse> getAllCourses() {
        List<OfferedCourse> courses = this.em
                .createNamedQuery(OfferedCourse.findAll, OfferedCourse.class)
                .getResultList();
        return courses;
    }
    
    @TransactionAttribute(SUPPORTS)
    public List<OfferedCourse> findCoursesBySeason(Season season) {
        List<OfferedCourse> courses = this.em
                .createNamedQuery(OfferedCourse.findAllBySeason, OfferedCourse.class)
                .setParameter("season", season)
                .getResultList();
        return courses;
    }
    
    @TransactionAttribute(SUPPORTS)
    public List<OfferedCourse> findCurrentCourses() {
        Season season = findCurrentSeason();
        return findCoursesBySeason(season);
    }
    
    @TransactionAttribute(SUPPORTS)
    public List<CourseReservation> findCurrentReservations() {
        Season season = findCurrentSeason();
        List<CourseReservation> bookings = this.em
                .createNamedQuery(CourseReservation.findCurrentReservations, CourseReservation.class)
                .setParameter("season", season)
                .getResultList();
        return bookings;
    }
    
    /**
     * Finds reservations of substitutes of the same course as the reservation with the reservationId has
     * @param courseId the ID of the offered course
     * @return list of substitutes
     */
    @TransactionAttribute(SUPPORTS)
    public List<CourseReservation> findSubstReservations(long courseId) {
        OfferedCourse course = this.em.find(OfferedCourse.class, courseId);
        List<CourseReservation> substReservations = this.em
                .createNamedQuery(CourseReservation.findReservationsByCourseAndRank, CourseReservation.class)
                .setParameter("course", course)
                .setParameter("rank", 0)
                .getResultList();
        return substReservations;
    }
    
    /**
     * Finds reservations of a particular course.
     * @param courseId the ID of the course
     * @return list of reservations
     */
    @TransactionAttribute(SUPPORTS)
    public List<CourseReservation> findCourseReservations(long courseId) {
        OfferedCourse course = this.em.find(OfferedCourse.class, courseId);
        List<CourseReservation> reservations = this.em
                .createNamedQuery(CourseReservation.findReservationsByCourseAndRank, CourseReservation.class)
                .setParameter("course", course)
                .setParameter("rank", course.getCapacity())
                .getResultList();
        return reservations;
    }
    
    /**
     * Finds participants' reservations of courses with specified name.
     * 
     * This is for list of participants
     * @param name the name of the course
     * @return list of reservations
     */
    @TransactionAttribute(SUPPORTS)
    public List<CourseReservation> findReservationsByCourseName(String name) {
        List<CourseReservation> reservations = this.em
                .createNamedQuery(CourseReservation.findReservationsByCourseName, CourseReservation.class)
                .setParameter("season", this.findCurrentSeason())
                .setParameter("name", name)
                .getResultList();
        return reservations;
    }
    
    public CourseReservation createCourseReservation(OfferedCourse course, Participant participant) {
        OfferedCourse c = this.em.find(OfferedCourse.class, course.getId());
        Participant p = this.em.find(Participant.class, participant.getId());
        CourseReservation r = new CourseReservation();
        r.setActive(true);
        r.setReservationTime(new Date());
        r.setCreatedBy(identityManager.getUser().getEmail());
        this.em.persist(r);
        r.setOfferedCourse(c);
        r.setParticipant(p);
        // compute rank and adjust balance
        short rank = (short)(c.getBalance() - 1);
        c.setBalance(rank);
        r.setRank(rank);
        this.em.flush();
        return r;
    }
 
    public CourseReservation createCourseReservation(long courseId, long participantId) {
        OfferedCourse course = this.em.find(OfferedCourse.class, courseId);
        Participant participant = this.em.find(Participant.class, participantId); 
        boolean alreadyPresent = this.em.createNamedQuery(CourseReservation.findReservationsByCourseAndRank, CourseReservation.class)
                .setParameter("course", course)
                .setParameter("rank", Short.MAX_VALUE)  // get participants with all ranks
                .getResultList()
                .stream()
                .map(CourseReservation::getParticipant)
                .anyMatch((p)-> p.equals(participant));
        if (alreadyPresent) {  // the participant is already present in the course
            return null;
        }
        return createCourseReservation(course, participant);
    }    
    
    /**
     * Deletes a course reservation.
     * 
     * The reservation can be deleted only if it is a reservation of a substitute or 
     * there are no substitutes in the offered course.
     * 
     * @param reservationId Course reservation ID to delete.
     * @return false, if the reservation cannot be deleted
     */
    public boolean deleteReservation(long reservationId) {
        CourseReservation res = this.em.find(CourseReservation.class, reservationId);
        
        OfferedCourse course = res.getOfferedCourse();
        if (res.getRank() >= 0 && course.getBalance() < 0) {
            return false;
        }
        
        // increment ranks of participants and substitutes with a lower rank
        this.em.createNamedQuery(CourseReservation.findReservationsByCourseAndRank, CourseReservation.class)
                .setParameter("course", course)
                .setParameter("rank", res.getRank())
                .getResultList()
                .forEach(CourseReservation::incRank);
        
        
        // increment the course balance
        short balance = course.getBalance();
        course.setBalance(++balance);
        
        // remove the reservation
        this.em.remove(res);
        
        return true;
    }

    /**
     * Replace a participant with a substitute and deletes the reservation of an original participant.
     * 
     * Use only for deleting participant's reservation if there are some substitutes for the course. 
     * 
     * @param toDeleteId - ID of the reservation to delete (participant's reservation)
     * @param replacementId  - ID of the reservation of the substitute
     */
    public void replaceAndDeleteReservation(long toDeleteId, long replacementId) {
        CourseReservation toDelete = this.em.find(CourseReservation.class, toDeleteId);
        CourseReservation replacement = this.em.find(CourseReservation.class, replacementId);
        short rank = toDelete.getRank();
        toDelete.setRank(replacement.getRank());
        replacement.setRank(rank);
        deleteReservation(toDeleteId);
    }
    
    public int findNumberOfNewReservations(Date since) {
        long numberOfReservations = this.em
                .createNamedQuery(CourseReservation.numberOfReservationsFromDate, Long.class)
                .setParameter("season", this.findCurrentSeason())
                .setParameter("since", since)
                .getSingleResult();
        return (int)numberOfReservations;
   }
    
    public void deleteCourse(long courseId) {
        OfferedCourse course = this.em.find(OfferedCourse.class, courseId);
        this.em.remove(course);
    }
    
    public Season updateSeason(Season s) {
        return this.em.merge(s);
    }
    
    public Season updatePreviousSeason(int year, String name) {
        LocalDateTime local = LocalDateTime.of(year, 1, 1, 0, 0);
        Date begin = Date.from(local.toInstant(ZoneOffset.ofHours(1)));
        Season s = findPreviousSeason();
        s.setDateBegin(begin);
        s.setDateEnd(begin);
        s.setName(name);
        s.setModifiedBy(identityManager.getUser().getEmail());
        return this.em.merge(s);
    }
    
    @TransactionAttribute(SUPPORTS)
    public List<Instructor> findInstructors() {
        List<Instructor> instructors = this.em
                .createNamedQuery(Instructor.findAllActive)
                .getResultList();
        return instructors;
    }

    @TransactionAttribute(SUPPORTS)
    public Instructor findInstructor(long instrId) {
        return this.em.find(Instructor.class, instrId);
    }

    @TransactionAttribute(SUPPORTS)
    public int findNumberOfCoursesByInstructor(Instructor instructor) {
        Instructor instr = this.em.find(Instructor.class, instructor.getId());
        long numberOfCourses = this.em
                .createNamedQuery(OfferedCourse.numberOfCoursesByInstructor, Long.class)
                .setParameter("instructor", instr)
                .getSingleResult();
        return (int)numberOfCourses;
    }
    
    public void deleteInstructor(Instructor instructor) {
        Instructor instr = this.em.find(Instructor.class, instructor.getId());
        this.em.remove(instr);
    }
    
    /**
     * Create an instructor
     * @param instr Instructor to create
     */
    public void createInstructor(Instructor instr) {
        this.em.persist(instr);
    }
    
    /**
     * Update an insatructor
     * @param instr Instructor to update
     */
    public void updateInstructor(Instructor instr) {
        this.em.merge(instr);
    }
    
    /**
     * Create a course
     * @param course Course to create
     * @param instructorId ID of the instructor
     */
    public void createCourse(OfferedCourse course, long instructorId) {
        // TODO: check, if course.getId() == null
        course.setInstructor(this.em.find(Instructor.class, instructorId));
        this.em.persist(course);
    }
    
    /**
     * Update a course
     * @param course Course (detached) to update
     * @param instructorId ID of the instructor
     * @param newCapacity new course capacity
     * @param substituteIds set of substitute IDs (if chosen manually)
     */
    public void updateCourse(OfferedCourse course, long instructorId, short newCapacity, Set<Long> substituteIds) {
        // TODO: check, if course.getId() != null
        logger.log(Level.FINE, "Updating {0}.", course.toString());
        if (course.getInstructor().getId() != instructorId) {
            course.setInstructor(this.em.find(Instructor.class, instructorId));
        }
        OfferedCourse mergedCourse = this.em.merge(course);
        changeCourseCapacity(mergedCourse, (int)newCapacity, substituteIds);
        logger.log(Level.INFO, "{0} was updated.", mergedCourse.toString());
    }
    
    /**
     * Transfers a course to the updated season.
     * 
     * Some reservations should be preserved, others should be abandoned.
     * The course capacity should not be changed here.
     * 
     * @param course Course (detached) to update
     * @param instructorId instructorId ID of the instructor
     * @param preservedIds IDs of reservation to preserve 
     * @param abandonedIds IDs to reservation to remove
     */
    public void transferCourseToSeason(OfferedCourse course, long instructorId, Set<Long> preservedIds, Set<Long> abandonedIds) {
        logger.log(Level.FINE, "Transfering {0}.", course.toString());
        
        // update the course first (same capacity and no selected substitutes, but update ballance)
        updateCourse(course, instructorId, course.getCapacity(), Collections.EMPTY_SET);
        OfferedCourse mergedCourse = this.em.merge(course);

        // remove substitute reservations
        findSubstReservations(course.getId()).forEach( (r) ->  deleteReservation(r.getId()) );
        // TODO: should it create a history item? 
        
        Season previousSeason = findPreviousSeason();
        // remove the abandoned reservations 
        abandonedIds.forEach( (id) -> {
            CourseReservation reservation = findReservation(id);
            deleteReservation(reservation.getId());
            historyTool.addToHistory(reservation, previousSeason);
        });
        
        // process the preserved reservation
        preservedIds.forEach( (id) -> {
            CourseReservation reservation = findReservation(id);
            reservation.setPayment(null);
            reservation.setReservationTime(new Date());
            reservation.setModifiedBy(identityManager.getUser().getEmail());
            historyTool.addToHistory(reservation, previousSeason);
        });
        
        // balance
        
        logger.log(Level.INFO, "{0} was transferred to the updated season.", mergedCourse.toString());
    }
    
    /**
     * Change the capacity of the existing course and sends emails if
     * the substitute is changed to a paqrticipant or vice versa.
     * 
     * @param course Course (managed) to change capacity
     * @param newCapacity new required capacity
     * @param substituteIds set of substitute IDs
     */
    private void changeCourseCapacity (OfferedCourse course, int newCapacity, Set<Long> substituteIds) {
      
        int capacityDiff = newCapacity - course.getCapacity();
        
        if (capacityDiff == 0) {
            return;
        }
        
        List<CourseReservation> reservations = findCourseReservations(course.getId());
        
        // Process manually chosen substitutes, if any
        if (!substituteIds.isEmpty()) {
            // we will need to perform reservations lookups by participant ID and rank (substitutes only)
            Map<Long, CourseReservation> reservationsByPtcId = reservations.stream()
                    .filter( (r) -> r.getRank() < 0 )
                    .collect(Collectors.toMap(r -> r.getParticipant().getId(), Function.identity(), (r1, r2) -> null, LinkedHashMap::new));
            Map<Integer, CourseReservation> reservationsByRank = reservations.stream()
                    .filter( (r) -> r.getRank() < 0 )
                    .collect(Collectors.toMap(r -> (int)r.getRank(), Function.identity(), (r1, r2) -> null, LinkedHashMap::new));
            // swap ranks, if some manually chosen substitutes
            Iterator<Long> substituteIdsIter = substituteIds.iterator();
            for (short rank = -1; -rank <= substituteIds.size(); --rank) {
                CourseReservation newSubst = reservationsByPtcId.get(substituteIdsIter.next());
                CourseReservation origSubst = reservationsByRank.get((int)rank);
                if (newSubst == origSubst) {
                    continue;
                }
                short savedRank = newSubst.getRank();
                newSubst.setRank(rank);
                origSubst.setRank(savedRank);
            }
        }
        
        // process all the reservations, adjust ranks by capacity difference and send confirmation emails
        reservations.stream().forEach((r) -> {
            Short prevRank = r.getRank();
            r.setRank((short)(prevRank + capacityDiff));
            Map<String,Object> values = new HashMap<>();
            values.put("participname", r.getParticipant().getName().getFullName());
            values.put("coursename", course.getName());
            values.put("weekday", WeekDay.valueOf(course.getWeekDay()).getName());
            values.put("timebegin", String.format("%d:%02d", course.getMinuteBegin() / 60, course.getMinuteBegin() % 60));
            values.put("timeend", String.format("%d:%02d", course.getMinuteEnd() / 60, course.getMinuteEnd() % 60));
            String text;
            if (r.getRank() < 0 && prevRank >= 0) {
                // participant to substitute confirmation - no confirmation required in this case
                //text = StUtils.renderTemplate("bookingpart2substconfrm", values);
                //mailService.send2(r.getParticipant().getPerson().getUser().getEmail(), "Změna rezervace", text);
            } else if (r.getRank() >= 0 && prevRank < 0) {
                // substitute to participant confirmation
                text = StUtils.renderTemplate("bookingsubst2partconfrm", values);
                mailService.send2(r.getParticipant().getPerson().getUser().getEmail(), "Změna rezervace", text);
            }
        });
                    
        course.setBalance((short)(course.getBalance() + capacityDiff));
        course.setCapacity((short)newCapacity);
    }
    
}
