/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package biz.sykora.rezervis.ejb;

import biz.sykora.rezervis.jsf.util.DateUtils;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.sql.DataSource;

/**
 * Database backup and housekeeping maintenance.
 */
@Singleton
@Startup
public class MaintenanceService {
    private static final Logger logger = Logger.getLogger(MaintenanceService.class.getName());

    private static final String DB_BACKUP_PATH_PREFIX = "db.backup.path.prefix";
    
    @Inject
    ActivityLogger activityLogger;
    
    @Resource(mappedName="java:/jboss/datasources/RezervisDS")
    DataSource ds;
    
    /**
     * Database backup.
     * 
     * Attention! This method is H2 database specific.
     * @throws SQLException 
     */
    @Schedule(hour="3,15", persistent=false)
    @Lock(LockType.READ)
    public void doDatabaseBackup() throws SQLException {
        DateFormat df = DateUtils.dateTimeIsoCompactFormat();
        String sql = "script drop to '" + System.getProperty(DB_BACKUP_PATH_PREFIX, "/tmp/db") + df.format(new Date()) + ".sql.zip' compression zip charset 'UTF-8'";
        try (Connection conn = ds.getConnection()) {
            conn.prepareStatement(sql).execute();
            logger.log(Level.INFO, "Database backup done.");
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error during creating DB backup.", ex);
        }
    }
    
    /**
     * Activity log housekeeping procedure. 
     * 
     * Removes activity log records older than 30 days.
     */
    @Schedule(dayOfWeek="Sat", hour="4", persistent=false)
    public void doActivityLogHousekeeping() {
        int records = activityLogger.removeOldRecords();
        logger.log(Level.INFO, "Activity log housekeeping: {0} records deleted.", String.valueOf(records));
    }
    
    /**
     * Database backup files housekeeping.
     * 
     * Removes database backup files older than 30 days. 
     */
    @Schedule(dayOfWeek="Sun", hour="4", persistent=false)
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void doFilesHousekeeping() {
        String backupFilesPrefix = System.getProperty(DB_BACKUP_PATH_PREFIX, "/tmp/db");
        // find the name of the backup directory
        int lastSlash = backupFilesPrefix.lastIndexOf('/');
        String backupDir = backupFilesPrefix.substring(0, lastSlash);
        if (backupDir.charAt(0) == '~') {
            backupDir = System.getProperty("user.home") + backupDir.substring(1);
        }
        try {
            // list the files and delete the old ones
            Files.list(FileSystems.getDefault().getPath(backupDir))
                    .filter( (p) -> p.toFile().isFile() )
                    .filter( (p) -> p.toString().endsWith(".sql.zip") )
                    .filter( (p) -> p.toFile().lastModified() < Instant.now().minus(30, ChronoUnit.DAYS).toEpochMilli() )
                    .forEach( (p) -> { p.toFile().delete(); });
            logger.log(Level.INFO, "Database backup files housekeeping done in {0} directory.", backupDir);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Error during DB backup files housekeeping.", ex);
        }
    }
    
}
