/*
 * Copyright (c) 2014-2017, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package biz.sykora.rezervis.ejb;

import biz.sykora.rezervis.cdi.IdentityManager;
import biz.sykora.rezervis.model.Address;
import biz.sykora.rezervis.model.Audience;
import biz.sykora.rezervis.model.Gender;
import biz.sykora.rezervis.model.Name;
import biz.sykora.rezervis.model.OfferedCourse;
import biz.sykora.rezervis.model.Participant;
import biz.sykora.rezervis.model.Person;
import biz.sykora.rezervis.model.Role;
import biz.sykora.rezervis.model.User;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

@Stateless
public class UserService {
    private static final Logger logger = Logger.getLogger(UserService.class.getName());
    
    @Inject
    IdentityManager identityManager;
    
    @Inject
    ActivityLogger activityLogger;
    
    @PersistenceContext(unitName = "rezervisdb")
    EntityManager em;
    
    public void save(User u){
        // TODO: set modifiedBy
        this.em.persist(u);
    }
    
    public void saveNewUser(String email, String password) {
        User u = new User();
        u.setEmail(email);
        u.setRoles(Arrays.asList(Role.USER));
        u.setPassword(hashPassword(password));
        //u.setPassword(password); // for debugging
        u.setVerificationKey(UUID.randomUUID().toString());
        u.setCreatedBy("self-registration");
        this.em.persist(u);
    }

    /**
     * Removes a user from database.
     * 
     * Personal data and participants are removed as well (by cascading).
     * If the user's person has payments not related to reservations, the payments
     * are removed as well (by cascading).
     * User's participants must have no reservations.
     * 
     * @param u User who should be deleted. 
     */
    public void deleteUser(User u) {
        long id = u.getId();
        User usr = this.em.find(User.class, id);
        this.em.remove(usr);
        logger.log(Level.INFO, "User {0} deleted.", usr);
    }

    public void updatePassword(String email, String password) {
        User user = find(email);
        user.setModifiedBy("forgotten-password");
        user.setPassword(hashPassword(password));
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public List<User> getActiveUsers(){
        return this.em.createNamedQuery(User.findAllActive).getResultList();
    }
    
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public List<User> getAllUsers(){
        return this.em.createNamedQuery(User.findAll).getResultList();
    }
    
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public User find(String email) {
        List<User> users = this.em.createNamedQuery(User.findByEmail).setParameter("email", email).getResultList();
        if (users.isEmpty()) {
            return null;
        }
        return users.get(0);
    }
    
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public User findByEmail(String email) {
        List<User> users = this.em.createNamedQuery(User.findByEmail).setParameter("email", email).getResultList();
        return users.isEmpty() ? null : users.get(0);
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void setLastLogin(User user) {
        User u = find(user.getId());
        u.setLastLogin(new Date());
    }
    
    public User find(long id) {
        User u = this.em.find(User.class, id);
        Person p = u.getPerson(); 
        if (p != null) {
            this.em.refresh(p);
        }
        return u;
    }

    public User getRefreshedUser(User user) {
        User u = this.em.find(User.class, user.getId());
        this.em.refresh(u);
        return u;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Participant findParticipantSelf(Person person) {
        return this.em.createNamedQuery(Participant.findSelf, Participant.class).setParameter("person", person).getSingleResult();
    } 

    /**
     * 
     * @param person 
     * @return active children of the person
     */
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public List<Participant> getChildren(Person person) {
        return (List<Participant>) this.em.createNamedQuery(Participant.getChildren).setParameter("person", person).getResultList();
    }
    
    /**
     * 
     * @param person 
     * @return all the active participants
     */
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public List<Participant> findAllParticipants() {
        return this.em.createNamedQuery(Participant.findAllParticipants, Participant.class).getResultList();
    }
    
    // returns ID of the newly created child
    public long addChild(Person person, Name name, Gender gender, Date dateBorn) {
        Participant p = new Participant(name, gender, dateBorn);
        person.addParticipant(p);
        p.setCreatedBy(identityManager.getUser().getEmail());
        em.persist(p);
        em.merge(person);
        activityLogger.log(person.getUser().getEmail(),"CHILD ADDED", p.getName().toString());
        return p.getId();
    }
    
    public void updateChild(long id, Name name, Gender gender, Date dateBorn) {
        Participant p = (Participant) this.em.find(Participant.class, id);
        if (p == null) {
            throw new IllegalArgumentException("Participant id=" + id + "not found.");
        }
        p.setName(name);
        p.setGender(gender);
        p.setDateBorn(dateBorn);
        p.setModifiedBy(identityManager.getUser().getEmail());        
        activityLogger.log(p.getPerson().getUser().getEmail(),"CHILD MODIFIED", p.getName().toString());
    }
    
            
    public User confirmUser(String key) {
        try {
            User u = (User) this.em.createQuery("SELECT u from User u where u.verificationKey = :key").setParameter("key", key).getSingleResult();
            u.setVerificationKey(null);  // this makes user's email address confirmed
            logger.log(Level.INFO, "User {0} with verification key {1} was confirmed.", new Object[]{u.getEmail(), key});
            activityLogger.log(u.getEmail(), "USER VERIFIED", null);
            return u;
        } catch (NoResultException e) {
            logger.log(Level.WARNING, "Verification key {0} not found. Confirmation of a user failed.", key);
            return null;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public boolean verifyPassword(String hashedPassword, String clearPpassword) {
        return hashedPassword.equals(hashPassword(clearPpassword));
    }
    
    public void changePassword(long userId, String password) {
        User u = find(userId);
        u.setPassword(hashPassword(password));
        activityLogger.log(u.getEmail(), "USER PASSWD CHANGED", null);
    }
    
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public boolean findUserExist(String email) {
        try {
            List<User> users  = this.em.createNamedQuery(User.findByEmail).setParameter("email", email).getResultList();
            return !users.isEmpty();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Database exception during testing the existence of the user: " + email, e);
            return false;
        }
    }
    static String hashPassword(String password) {
        String hash= password;
            MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-256");
            md.update(password.getBytes(StandardCharsets.UTF_8));
            byte byteData[] = md.digest();
            hash = Base64.getEncoder().encodeToString(byteData);
        } catch (NoSuchAlgorithmException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
        return hash;
    }

    public void addOrUpdatePerson(long userId, Name name, Address address, String phone, String phone2, Gender gender, String yearBornStr) {
        User u = find(userId);
        Person p = u.getPerson();
        Calendar calBorn = Calendar.getInstance();
        int yearBorn = yearBornStr == null || yearBornStr.isEmpty()  ? 0 : Integer.valueOf(yearBornStr);
        Date born = null;
        if (yearBorn >= 1900 && yearBorn <= calBorn.get(Calendar.YEAR)) {
            calBorn.set(yearBorn, 0, 1);
            born = calBorn.getTime();
        }
        String createdOrModifiedBy = identityManager.getUser().getEmail();
        if (p == null) {
            p = new Person();
            p.setName(name);
            p.setGender(gender);
            p.setAddress(address);
            p.setPhone(phone);
            p.setPhone2(phone2);
            p.setYearBorn(yearBorn);
            p.setCreatedBy(createdOrModifiedBy);
            this.em.persist(p);
            Participant part = new Participant();
            part.setName(name);
            part.setSelf(true);
            part.setGender(gender);
            part.setDateBorn(born);
            part.setCreatedBy(createdOrModifiedBy);
            part.setPerson(p);
            this.em.persist(part);
            u.setPerson(p);
            activityLogger.log(createdOrModifiedBy, "PERSON CREATED", p.toString());
        } else {
            p.setName(name);
            p.setGender(gender);
            p.setAddress(address);
            p.setPhone(phone);
            p.setPhone2(phone2);
            p.setYearBorn(yearBorn);
            p.setModifiedBy(createdOrModifiedBy);
            Participant part = findParticipantSelf(p);
            part.setName(name);
            part.setSelf(true);
            part.setGender(gender);
            part.setDateBorn(born);
            part.setModifiedBy(createdOrModifiedBy);
            activityLogger.log(createdOrModifiedBy, "PERSON MODIFIED", p.toString());
        }
        this.em.flush();
    }
    
    public List<Participant> findBookingCandidates(Person person, OfferedCourse course) {
        logger.log(Level.INFO, "findBookingCandidates of {0} course {1}", new Object[]{person, course});
        // choose all the participants for now
        List<Participant> candidates = this.em.createNamedQuery(Participant.findParticipants)
                .setParameter("person", person)
                .getResultList();
        // find already registered
        List<Participant> registrants  = this.em.createNamedQuery(Participant.findRegistrants)
                .setParameter("person", person)
                .setParameter("offeredCourse", course)
                .getResultList();
        // remove already registered participants
        candidates.removeAll(registrants);
        // remove too young or too old participants
        if (course.getAudience() == Audience.CHILD || course.getAudience() == Audience.CHILD_PARENT) {
            // for children or children with parents
            candidates.removeIf( (p) -> p.isSelf() );
            long eighteenYears = new Date().getTime() - Math.round(60*60*24*365.25*1000*18);
            candidates.removeIf( (p) -> p.getDateBorn().getTime() < eighteenYears ); // remove children over 18
        } else if (course.getAudience() == Audience.ADULT || course.getAudience() == Audience.TEEN || course.getAudience() == Audience.DO || course.getAudience() == Audience.SENIN) {
            // for adults and oldish children
            long eightYears = new Date().getTime() - Math.round(60*60*24*365.25*1000*8);
            candidates.removeIf( (p) -> p.getDateBorn() != null && p.getDateBorn().getTime() > eightYears ); // remove children under 8
        }
        return candidates;
    }

    public List<Participant> findRegistrants(Person person, OfferedCourse course) {
        logger.log(Level.INFO, "findRegistrants of {0} course {1}", new Object[]{person, course});
        List<Participant> registrants  = this.em.createNamedQuery(Participant.findRegistrants)
                .setParameter("person", person)
                .setParameter("offeredCourse", course)
                .getResultList();
        return registrants;
    }
    
    public void updateUserRoles(User user, List<String> rolesStr) {
        User u = this.em.find(User.class, user.getId());
        List<Role> roles = rolesStr.stream().map(Role::valueOf).collect(Collectors.toList());
        u.setRoles(roles);
    }
}
