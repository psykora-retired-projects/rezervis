/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package biz.sykora.rezervis.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "OFFERED_COURSES")
@NamedQueries({
    @NamedQuery(name=OfferedCourse.findAll,query="SELECT c from OfferedCourse c ORDER BY c.weekDay, c.minuteBegin"),
    @NamedQuery(name=OfferedCourse.findAllBySeason,query="SELECT c FROM OfferedCourse c WHERE c.season = :season ORDER BY c.weekDay, c.minuteBegin, c.name"),
    @NamedQuery(name=OfferedCourse.findOfferedByAudience,query="SELECT c from OfferedCourse c where c.audience = :audience and c.season.current = TRUE and c.active = TRUE order by c.weekDay, c.minuteBegin"),
    @NamedQuery(name=OfferedCourse.numberOfCoursesByInstructor,query="SELECT count(c.id) FROM OfferedCourse c WHERE c.instructor = :instructor"),
    @NamedQuery(name=OfferedCourse.findExpiredCourses,query="SELECT c FROM OfferedCourse c WHERE c.dateEnd < c.season.dateBegin ORDER BY c.weekDay, c.minuteBegin, c.name")
})
public class OfferedCourse extends AuditedEntity {
    public final static String findAll = "biz.sykora.entities.OfferedCourse.findAll";
    public final static String findAllBySeason = "biz.sykora.entities.OfferedCourse.findAllBySeason";
    public final static String findOfferedByAudience = "biz.sykora.entities.OfferedCourse.findOfferedByAudience";
    public final static String findExpiredCourses = "biz.sykora.entities.OfferedCourse.findExpiredCourses";
    public final static String numberOfCoursesByInstructor = "biz.sykora.entities.OfferedCourse.numberOfCoursesByInstructor";

    @ManyToOne(optional=false)
    private Season season;

    @NotNull
    private String name;

    @Enumerated(EnumType.STRING)
    private Audience audience;
    
    private String age;

    @ManyToOne(optional=false)
    private Instructor instructor;
    
    @Min(1)
    @Max(7)
    @Column(name = "WEEK_DAY")
    private short weekDay;
    
    // Daytime in minutes - must be converted to HH:MM for users
    @Column(name = "MINUTE_BEGIN")
    @Min(0)
    @Max(1439)
    private short minuteBegin;
    
    // Daytime in minutes - must be converted to HH:MM for users
    @Column(name = "MINUTE_END")
    @Min(0)
    @Max(1439)
    private short minuteEnd;
        
    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_BEGIN")
    private Date dateBegin;

    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_END")
    private Date dateEnd;

    @Column(name = "NUMBER_OF_LESSONS")
    private short numberOfLessons;
    
    private short price;
    
    // max number of participants
    private short capacity;
    
    // actual number of participants
    private short balance;

    private String description;
    
    // ID of the course page on the web pages
    @Column(name = "ID_EXT")
    private int idExt;
    
    // External URL of the course page
    @Column(name = "URL_EXT")
    private String urlExt;
    
    public OfferedCourse() {
        super();
    }

    public Season getSeason() {
        return season;
    }

    public void setSeason(Season season) {
        this.season = season;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Audience getAudience() {
        return audience;
    }

    public void setAudience(Audience audience) {
        this.audience = audience;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public Instructor getInstructor() {
        return instructor;
    }

    public void setInstructor(Instructor instructor) {
        this.instructor = instructor;
    }

    public short getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(short weekDay) {
        this.weekDay = weekDay;
    }

    public short getMinuteBegin() {
        return minuteBegin;
    }

    public void setMinuteBegin(short minuteBegin) {
        this.minuteBegin = minuteBegin;
    }

    public short getMinuteEnd() {
        return minuteEnd;
    }

    public void setMinuteEnd(short minuteEnd) {
        this.minuteEnd = minuteEnd;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public short getNumberOfLessons() {
        return numberOfLessons;
    }

    public void setNumberOfLessons(short numberOfLessons) {
        this.numberOfLessons = numberOfLessons;
    }

    public short getPrice() {
        return price;
    }

    public void setPrice(short price) {
        this.price = price;
    }

    public short getCapacity() {
        return capacity;
    }

    public void setCapacity(short capacity) {
        this.capacity = capacity;
    }

    public short getBalance() {
        return balance;
    }

    public void setBalance(short balance) {
        this.balance = balance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIdExt() {
        return idExt;
    }

    public void setIdExt(int idExt) {
        this.idExt = idExt;
    }

    public String getUrlExt() {
        if (urlExt == null) {
            return null;
        }
        if (urlExt.startsWith("http")) {
            return urlExt;
        }
        return "http://www.brezanek.cz" + urlExt; // TODO: hardwired constant
    }

    public void setUrlExt(String urlExt) {
        this.urlExt = urlExt;
    }

    @Override
    public String toString() {
        return "OfferedCourse{" + "id=" + getId() + ", name=" + name + '}';
    }

}
