/*
 * Copyright (c) 2014-2017, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package biz.sykora.rezervis.model;


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="SEASONS")
@NamedQueries({
    @NamedQuery(name=Season.findCurrent,query="SELECT s from Season s where s.current = TRUE"),
    @NamedQuery(name=Season.findPrevious,query="SELECT s from Season s where s.current = FALSE")
})
public  class Season  extends AuditedEntity {
    public final static String findCurrent = "biz.sykora.entities.Season.findCurrent";
    public final static String findPrevious = "biz.sykora.entities.Season.findPrevious";

    private boolean current;
 
    @NotNull
    private String name;
    
    @Temporal(TemporalType.DATE)
    @NotNull
    @Column(name = "DATE_BEGIN")    
    private Date dateBegin;

    @Temporal(TemporalType.DATE)
    @NotNull
    @Column(name = "DATE_END")    
    private Date dateEnd;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "RESERVATION_START")  
    private Date reservationStart;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "RESERVATION_START2")
    private Date reservationStart2;

    private String description; 

    public Season() {
        super();
    }

    public Season(String name, Date dateBegin, Date dateEnd) {
        super();
        this.name = name;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
    }

    public boolean isCurrent() {
        return current;
    }

    public void setCurrent(boolean current) {
        this.current = current;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Date getReservationStart() {
        return reservationStart;
    }

    public void setReservationStart(Date reservationStart) {
        this.reservationStart = reservationStart;
    }

    public Date getReservationStart2() {
        return reservationStart2;
    }

    public void setReservationStart2(Date reservationStart2) {
        this.reservationStart2 = reservationStart2;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    @Override
    public String toString() {
        return "Season{" + "id=" + getId() + ", name=" + name + '}';
    }

    
}

