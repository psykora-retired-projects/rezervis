/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package biz.sykora.rezervis.model;

import java.util.Comparator;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "RESERVATIONS")  
@NamedQueries({
    @NamedQuery(name = CourseReservation.findCurrentReservations, query = "SELECT cr FROM CourseReservation cr WHERE cr.offeredCourse.season = :season ORDER BY cr.offeredCourse.weekDay, cr.offeredCourse.minuteBegin, cr.offeredCourse.name, cr.rank DESC"),
    @NamedQuery(name = CourseReservation.findReservationsByCourseAndRank, query ="SELECT cr FROM CourseReservation cr WHERE cr.offeredCourse = :course AND cr.rank < :rank ORDER BY cr.rank DESC"),
    @NamedQuery(name = CourseReservation.findReservationsByCourseName, query ="SELECT cr FROM CourseReservation cr WHERE cr.offeredCourse.season = :season AND cr.offeredCourse.name = :name AND cr.rank >= 0 ORDER BY cr.offeredCourse.weekDay, cr.offeredCourse.minuteBegin, cr.rank DESC"),
    @NamedQuery(name = CourseReservation.numberOfReservationsFromDate, query ="SELECT count(cr.id) FROM CourseReservation cr WHERE cr.offeredCourse.season = :season AND cr.reservationTime >= :since")
})
public class CourseReservation extends AuditedEntity {
    public final static String findCurrentReservations = "biz.sykora.rezervis.model.CourseReservation.findCurrentReservations";
    public final static String findReservationsByCourseAndRank = "biz.sykora.rezervis.model.CourseReservation.findReservationsByCourseAndRank";
    public final static String findReservationsByCourseName = "biz.sykora.rezervis.model.CourseReservation.findReservationsByCourseName";
    public final static String numberOfReservationsFromDate = "biz.sykora.rezervis.model.CourseReservation.numberOfReservationsFromDate";

    @ManyToOne
    private Participant participant;

    @ManyToOne
    @JoinColumn(name = "OFFERED_COURSE_ID")
    private OfferedCourse offeredCourse;

    @ManyToOne
    private Payment payment;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "RESERVATION_TIME")
    private Date reservationTime;

    private short rank;

    public CourseReservation() {
        super();
    }

    public Participant getParticipant() {
        return participant;
    }

    public void setParticipant(Participant participant) {
        this.participant = participant;
//        if (!participant.getCourseReservations().contains(this)) {
//            participant.getCourseReservations().add(this);
//        }        
    }

    public OfferedCourse getOfferedCourse() {
        return offeredCourse;
    }

    public void setOfferedCourse(OfferedCourse offeredCourse) {
        this.offeredCourse = offeredCourse;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
        if (payment == null) {
            if (this.payment != null) {
                this.payment.removeCourseReservation(this);
            }
        } else if (!payment.getCourseReservations().contains(this)) {
            payment.getCourseReservations().add(this);
        }
    }

    public Date getReservationTime() {
        return reservationTime;
    }

    public void setReservationTime(Date reservationTime) {
        this.reservationTime = reservationTime;
    }

    public short getRank() {
        return rank;
    }

    public void setRank(short rank) {
        this.rank = rank;
    }

    /** 
     * Rank increment.
     */
    public void incRank() {
        this.rank++;
    }

    @Override
    public String toString() {
        return "CourseReservation{" + "id=" + getId() + '}';
    }

    public static class TimeComparator implements Comparator<CourseReservation> {

        @Override
        public int compare(CourseReservation o1, CourseReservation o2) {
            if (o1.getOfferedCourse() == null || o2.getOfferedCourse() == null) {
                return 0;
            }
            return o1.getOfferedCourse().getWeekDay() * 2000
                    + o1.getOfferedCourse().getMinuteBegin()
                    - o2.getOfferedCourse().getWeekDay() * 2000
                    - o2.getOfferedCourse().getMinuteBegin();
        }
    }
    
}
