/*
 * Copyright (c) 2014-2016, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package biz.sykora.rezervis.model;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Past;

@Entity
@Table(name="PARTICIPANTS",indexes = { @Index(columnList = "FAMILY_NAME"), @Index(columnList = "PERSON_ID") })
@NamedQueries({
    @NamedQuery(name=Participant.findSelf,query="SELECT p from Participant p where p.person = :person and p.self = TRUE"),
    @NamedQuery(name=Participant.getChildren,query="SELECT p from Participant p where p.person = :person and p.active = TRUE and p.self <> TRUE"),
    @NamedQuery(name=Participant.findParticipants,query="SELECT p from Participant p where p.person = :person and p.active = TRUE"),
    @NamedQuery(name=Participant.findAllParticipants,query="SELECT p from Participant p where p.active = TRUE order by p.name.familyName, p.name.firstName"),
    @NamedQuery(name=Participant.findBookingCandidates,
            query="SELECT DISTINCT p FROM Participant p LEFT JOIN p.courseReservations r WHERE p.person = :person AND p.active = TRUE AND (r.offeredCourse != :offeredCourse OR r.offeredCourse IS NULL) ORDER BY p.dateBorn"),
    @NamedQuery(name=Participant.findRegistrants,
            query="SELECT p FROM Participant p JOIN p.courseReservations r WHERE p.person = :person AND p.active = TRUE AND r.offeredCourse = :offeredCourse ORDER BY p.dateBorn")
})
public class Participant extends AuditedEntity {
    public final static String findSelf = "biz.sykora.booking.model.Participant.findSelf";
    public final static String getChildren = "biz.sykora.booking.model.Participant.getChildren";
    public final static String findParticipants = "biz.sykora.booking.model.Participant.findParticipants";
    public final static String findAllParticipants = "biz.sykora.booking.model.Participant.findAllParticipants";
    public final static String findBookingCandidates = "biz.sykora.booking.model.Participant.findBookingCandidates";
    public final static String findRegistrants = "biz.sykora.booking.model.Participant.findRegistrants";

    @ManyToOne(optional=false)
    private Person person;
    
    private boolean self = false; // is it the person?
    
    @Embedded
    private Name name;
    
    @Enumerated(EnumType.STRING)
    private Gender gender;
    
    @Temporal(TemporalType.DATE)
    @Past
    @Column(name = "DATE_BORN")
    private Date dateBorn;

    @OneToMany(mappedBy="participant", fetch = FetchType.EAGER)
    private List<CourseReservation> courseReservations = new ArrayList<>();
    
    public Participant() {
    }
    
    public Participant(Name name, Gender gender, Date dateBorn) {
        this.name = name;
        this.gender = gender;
        this.dateBorn = dateBorn; 
    }
    
    public Person getPerson() {
        return this.person;
    }


    public void setPerson (Person person) {
        this.person = person;
        if (!person.getParticipants().contains(this)) {
            person.getParticipants().add(this);
        }
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public Date getDateBorn() {
        return dateBorn;
    }

    public void setDateBorn(Date dateBorn) {
        this.dateBorn = dateBorn;
    }

    public boolean isSelf() {
        return self;
    }

    public void setSelf(boolean self) {
        this.self = self;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public List<CourseReservation> getCourseReservations() {
        List<CourseReservation> sorted = new ArrayList<>(courseReservations);
        Collections.sort(sorted, new CourseReservation.TimeComparator());
        return sorted;
    }

    public void setCourseReservations(List<CourseReservation> courseReservations) {
        this.courseReservations = courseReservations;
    }

    public void addCourseReservation(CourseReservation courseReservation) {
        this.courseReservations.add(courseReservation);
        if (courseReservation.getParticipant() != this) {
            courseReservation.setParticipant(this);
        }
    }
    
    @Override
    public String toString() {
        return "Participant{" + "id=" + getId() + ", name=" + name + '}';
    }

}

