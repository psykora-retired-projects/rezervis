/*
 * Copyright (c) 2014-2017, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package biz.sykora.rezervis.model;


import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="PERSONS",indexes = { @Index(columnList = "FAMILY_NAME") }) 
@NamedQueries({
    @NamedQuery(name=Person.findAll,query="SELECT p from Person p"),
    @NamedQuery(name=Person.findAllActive,query="SELECT p from Person p where p.active = TRUE")
})
public  class Person extends AuditedEntity {
    public final static String findAll = "biz.sykora.entities.Person.findAll";
    public final static String findAllActive = "biz.sykora.entities.Person.findAllActive";

    @OneToOne(mappedBy="person", optional=false)
    private User user;
    
    @Embedded
    @NotNull
    private Name name;

    @Enumerated(EnumType.STRING)
    private Gender gender;
    
    @Embedded
    private Address address;
    
    @NotNull
    private String phone;

    private String phone2;
    
    @Min(0)
    @Max(2010)
    @Column(name = "YEAR_BORN")
    private int yearBorn;


    @OneToMany(mappedBy="person", fetch = FetchType.EAGER, cascade={CascadeType.REFRESH, CascadeType.REMOVE})
    private List<Participant> participants = new ArrayList<>();

    @OneToMany(mappedBy="person", cascade={CascadeType.REFRESH, CascadeType.REMOVE})
    private List<Payment> payments = new ArrayList<>();

    @OneToMany(mappedBy="person", fetch = FetchType.EAGER, cascade={CascadeType.REFRESH, CascadeType.REMOVE})
    private List<Membership> membership = new ArrayList<>();

    private boolean complete;
    
    public Person() {
    }

    public Person(User user, Name name, Address address, String phone) {
        this.setUser(user);
        this.name = name;
        this.address = address;
        this.phone = phone;
    }
    
    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public List<Participant> getParticipants() {
        return this.participants;
    }

    public void setParticipants (List<Participant> participants) {
        this.participants = participants;
    }
    
    public final void addParticipant(Participant participant) {
        this.participants.add(participant);
        if (participant.getPerson() != this) {
            participant.setPerson(this);
        }
    }

    public Address getAddress() {
        return this.address;
    }

    public void setAddress (Address address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public int getYearBorn() {
        return yearBorn;
    }

    public void setYearBorn(int yearBorn) {
        this.yearBorn = yearBorn;
    }

    public List<Payment> getPayments() {
        return this.payments;
    }


    public void setPayments (List<Payment> payments) {
        this.payments = payments;
    }
    
    public final void addPayment(Payment payment) {
        this.payments.add(payment);
        if (payment.getPerson() != this) {
            payment.setPerson(this);
        }
    }

   public List<Membership> getMembership() {
        return this.membership;
    }


    public void setMembership (List<Membership> membership) {
        this.membership = membership;
    }

    public final void addMembership(Membership membership) {
        this.membership.add(membership);
        if (membership.getPerson() != this) {
            membership.setPerson(this);
        }
    }
    
    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }
    
    public User getUser() {
        return user;
    }

    public final void setUser(User user) {
        this.user = user;
        if (user.getPerson() != this) {
            user.setPerson(this);
        }
    }

    @Override
    public String toString() {
        return "Person{" + "id=" + id + ", name=" + name + ", address=" + address + '}';
    }
}

