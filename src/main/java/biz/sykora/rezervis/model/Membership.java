/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package biz.sykora.rezervis.model;


import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="MEMBERSHIP")
public  class Membership extends AuditedEntity {

    @Temporal(TemporalType.DATE)
    @NotNull
    @Column(name = "DATE_BEGIN")
    private Date dateBegin;

    @Temporal(TemporalType.DATE)
    @NotNull
    @Column(name = "DATE_END")
    private Date dateEnd;

    @Column(name="CARD_NUMBER")
    private String cardNumber;
    
    @OneToOne
    private Payment payment;

    @ManyToOne(optional=false)
    @NotNull
    private Person person;

    
    public Membership() {}
    
    public Membership(Person p, int year, int halfYear) {
        this.person = p;
        int monthBegin = halfYear == 1 ? Calendar.JANUARY : Calendar.JULY;
        this.dateBegin = new GregorianCalendar(year, monthBegin, 1).getTime();
        if (halfYear == 1) {
            this.dateEnd = new GregorianCalendar(year, Calendar.JUNE, 30).getTime();
        } else {  // TODO: check, if halfYear == 2
            this.dateEnd = new GregorianCalendar(year, Calendar.DECEMBER, 31).getTime();
        }        
    }

    public Date getDateBegin() {
        return this.dateBegin;
    }


    public void setDateBegin (Date dateBegin) {
        this.dateBegin = dateBegin;
    }


    public Date getDateEnd() {
        return this.dateEnd;
    }


    public void setDateEnd (Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Payment getPayment() {
        return this.payment;
    }


    public void setPayment (Payment payment) {
        this.payment = payment;
    }


    public Person getPerson() {
        return this.person;
    }


    public void setPerson (Person person) {
        this.person = person;
    }

    public int getYear() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateBegin);
        return cal.get(Calendar.YEAR);
    }
    
    public int getHalfYear() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateBegin);
        return cal.get(Calendar.MONTH) == Calendar.JANUARY ? 1 : 2;
    }
    
    public String getSeason() {
        return String.valueOf(getYear()) + "/" + String.valueOf(getHalfYear()); 
    }
    
}

