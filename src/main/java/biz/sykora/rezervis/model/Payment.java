/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package biz.sykora.rezervis.model;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

@Entity
@Table(name="PAYMENTS")
@NamedQueries({
    @NamedQuery(name=Payment.findAll,query="SELECT p from Payment p"),
    @NamedQuery(name=Payment.findAllActive,query="SELECT p FROM Payment p WHERE p.active = TRUE ORDER BY p.paymentDate DESC")
})
public  class Payment  extends AuditedEntity {
    public final static String findAll = "biz.sykora.rezervis.model.Payment.findAll";
    public final static String findAllActive = "biz.sykora.rezervis.model.Payment.findAllActive";

    @Temporal(TemporalType.DATE)
    @Past
    @NotNull
    @Column(name = "PAYMENT_DATE")    
    private Date paymentDate;

    @NotNull
    private int amount;

    @ManyToOne(optional=false,targetEntity=Person.class)
    @NotNull
    private Person person;

    @OneToMany(mappedBy="payment", fetch = FetchType.EAGER, cascade={CascadeType.REFRESH})
    private List<CourseReservation> courseReservations = new ArrayList<>();

    @Enumerated(EnumType.STRING)
    @Column(name = "PAYMENT_TYPE")    
    private PaymentType paymentType;
    

    public Date getPaymentDate() {
        return this.paymentDate;
    }

    public void setPaymentDate (Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public int getAmount() {
        return this.amount;
    }

    public void setAmount (int amount) {
        this.amount = amount;
    }

    public Person getPerson() {
        return this.person;
    }

    public void setPerson (Person person) {
        this.person = person;
        if (!person.getPayments().contains(this)) {
            person.getPayments().add(this);
        }
    }

    public List<CourseReservation> getCourseReservations() {
        return courseReservations;
    }

    public void setCourseReservations(List<CourseReservation> courseReservations) {
        this.courseReservations = courseReservations;
    }

    public void addCourseReservations(CourseReservation courseReservation) {
        this.courseReservations.add(courseReservation);
        if (courseReservation.getPayment() != this) {
            courseReservation.setPayment(this);
        }
    }

    public void removeCourseReservation(CourseReservation courseReservation) {
        this.courseReservations.remove(courseReservation);
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    // convenience methods
    
    public String getPersonName() {
        if (person == null) {
            return null;
        }
        return person.getName().getFullName();
    }
    
    @Override
    public String toString() {
        return "Payment{" + "paymentDate=" + paymentDate + ", amount=" + amount + ", paymentType=" + paymentType + '}';
    }

    
}

