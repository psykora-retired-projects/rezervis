/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package biz.sykora.rezervis.model;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="USERS", indexes = {@Index(columnList = "EMAIL")})
@NamedQueries({
    @NamedQuery(name=User.findAll,query="SELECT u from User u"),
    @NamedQuery(name=User.findAllActive,query="SELECT u from User u where u.active = TRUE"),
    @NamedQuery(name=User.findOne,query="SELECT u from User u where u.email = :email and u.password = :password"),
    @NamedQuery(name=User.findByEmail,query="SELECT u from User u where u.email = :email")
})
public class User extends AuditedEntity {
    public final static String findAll = "biz.sykora.entities.User.findAll";
    public final static String findAllActive = "biz.sykora.entities.User.findAllActive";
    public final static String findOne = "biz.sykora.entities.User.findOne";
    public final static String findByEmail = "biz.sykora.entities.User.findByEmail";
    
    @Column(name="EMAIL", unique = true)
    @NotNull
    private String email; // this is also username for login

    @NotNull
    private String password;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="LAST_LOGIN")
    private Date lastLogin;
    
    @ElementCollection(targetClass=Role.class, fetch=FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    @CollectionTable(name="USER_ROLES", joinColumns={@JoinColumn(name="USER_ID")})
    @Column(name="ROLE")
    private List<Role> roles;
    
    @OneToOne(fetch=FetchType.EAGER, cascade={CascadeType.REFRESH, CascadeType.REMOVE} )
    private Person person; 

    @Column(name="VERIFICATION_KEY")
    private String verificationKey;
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
        if (person.getUser() != this) {
            person.setUser(this);
        }
    }

    public String getVerificationKey() {
        return verificationKey;
    }

    public void setVerificationKey(String verificationKey) {
        this.verificationKey = verificationKey;
    }
    
    public boolean isVerified() {
        return this.verificationKey == null;
    }
    
    public boolean isInRole(String role) {
        try {
            return roles.contains(Role.valueOf(role));
        } catch (Exception e) {
            return false;
        }
    }
    
    /**
     * Sets a user active or not.
     * 
     * The flag is also spread to the related person.
     * 
     * @param active true, if the user/person/participants are active, false otherwise 
     */
    @Override
    public void setActive(boolean active) {
        super.setActive(active); 
        if (this.person != null) {
            this.person.setActive(active);            
        }
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof User)) {
            return false;
        }
        User other = (User) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "User{" + "email=" + email + '}';
    }

}
