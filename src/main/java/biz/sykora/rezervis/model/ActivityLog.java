/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package biz.sykora.rezervis.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

@Entity
@Table(name="ACTIVITY_LOG")
@Cacheable(false)
public class ActivityLog implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date ts;

    @Size(max = 255)  // Shorten in setter
    @Column(name = "NAME", length = 20)
    private String name;

    @Size(max = 255)  // Shorten in setter
    @Column(name = "ACTION", length = 20)
    private String action;
    
    @Column(name = "DESCRIPTION", length = 100)
    private String description;

    public ActivityLog() {}

    public ActivityLog(String name, String action, String description) {
        this.name = first20(name);
        this.action = first20(action);
        this.description = first100(description);
    }
    
    public long getId() {
        return id;
    }

    public Date getTs() {
        return ts;
    }

    public void setTs(Date ts) {
        this.ts = ts;
    }

    public String getName() {
            return name;
    }

    public void setName(String name) {
            this.name = first20(name);
    }

    public String getAction() {
            return action;
    }

    public void setAction(String description) {
            this.action = first20(description);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = first100(description);
    }

    /**
     * Sets createdAt before insert
     */
    @PrePersist
    public void setCreationDate() {
        this.ts = new Date();
    }

    private static String first20(String str) {
        if (str == null || str.length() <= 20) {
            return str;
        } else {
            return str.substring(0, 20);
        }
    }

    private static String first100(String str) {
        if (str == null || str.length() <= 100) {
            return str;
        } else {
            return str.substring(0, 100);
        }
    }
}
