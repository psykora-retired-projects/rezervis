/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package biz.sykora.rezervis.model;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="HISTORY")
@NamedQueries({
    @NamedQuery(name=HistoryItem.findAll,query="SELECT h from HistoryItem h"),
})
public  class HistoryItem  implements Serializable {
    public final static String findAll = "biz.sykora.rezervis.model.HistoryItem.findAll";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @NotNull
    @Column(name = "SEASON_YEAR")
    private int seasonYear;

    @NotNull
    @Column(name = "SEASON_NAME")
    private String seasonName;

    @NotNull
    @Column(name = "USER_EMAIL")
    private String userEmail;

    @NotNull
    @Column(name = "PERSON_NAME")
    private String personName;

    @NotNull
    @Column(name = "PARTICIPANT_NAME")
    private String participantName;

    @Column(name = "PARTICIPANT_AGE")
    private int participantAge;

    @NotNull
    @Column(name = "COURSE_NAME")
    private String courseName;

    @Column(name = "COURSE_ID")
    private long courseId;

    public HistoryItem() {}
    
    public HistoryItem(int year, String season, String email, String name, String partName, int age, String courseName, long courseId) {
        this.seasonYear = year;
        this.seasonName = season;
        this.userEmail = email;
        this.personName = name;
        this.participantName = partName;
        this.participantAge = age;
        this.courseName = courseName;
        this.courseId = courseId;
    }
    
    public HistoryItem(int year, String season, String email, String name, String partName, int age, String courseName) {
        this(year, season, email, name, partName, age, courseName, -1);
    }
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getSeasonYear() {
        return seasonYear;
    }

    public void setSeasonYear(int seasonYear) {
        this.seasonYear = seasonYear;
    }

    public String getSeasonName() {
        return seasonName;
    }

    public void setSeasonName(String seasonName) {
        this.seasonName = seasonName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String pearsonName) {
        this.personName = pearsonName;
    }

    public String getParticipantName() {
        return participantName;
    }

    public void setParticipantName(String participantName) {
        this.participantName = participantName;
    }

    public int getParticipantAge() {
        return participantAge;
    }

    public void setParticipantAge(int participantAge) {
        this.participantAge = participantAge;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public long getCourseId() {
        return courseId;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }
        
}

