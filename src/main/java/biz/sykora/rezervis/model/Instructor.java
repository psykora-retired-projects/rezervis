/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package biz.sykora.rezervis.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "INSTRUCTORS")
@NamedQueries({
    @NamedQuery(name = Instructor.findAll, query = "SELECT i FROM Instructor i"),
    @NamedQuery(name = Instructor.findAllActive, query = "SELECT i FROM Instructor i WHERE i.active = TRUE ORDER BY i.name.familyName, i.name.firstName")
})
public class Instructor extends AuditedEntity {

    public final static String findAll = "biz.sykora.rezervis.entities.Instructor.findAll";
    public final static String findAllActive = "biz.sykora.rezervis.entities.Instructor.findAllActive";

    @Embedded
    @NotNull
    private Name name;

    private String description;

    // ID of the instructor page on the web pages
    @Column(name = "ID_EXT")
    private int idExt;

    // External URL of the instructor page
    @Column(name = "URL_EXT")
    private String urlExt;

    public Instructor() {
        super();
    }

    public Instructor(Name name, String description, int idExt, String urlExt) {
        super();
        this.name = name;
        this.description = description;
        this.idExt = idExt;
        this.urlExt = urlExt;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIdExt() {
        return idExt;
    }

    public void setIdExt(int idExt) {
        this.idExt = idExt;
    }

    public String getUrlExt() {
        if (urlExt.startsWith("http")) {
            return urlExt;
        }
        return "http://www.brezanek.cz" + urlExt;  // TODO: hardwired constant
    }

    public void setUrlExt(String urlExt) {
        this.urlExt = urlExt;
    }

    @Override
    public String toString() {
        return "Instructor{" + "id=" + id + ", name=" + name + '}';
    }
}
