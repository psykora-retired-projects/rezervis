/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package biz.sykora.rezervis.cdi;

import biz.sykora.rezervis.ejb.ActivityLogger;
import biz.sykora.rezervis.ejb.UserService;
import biz.sykora.rezervis.model.User;
import java.io.Serializable;
import java.security.Principal;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

@Named("identityManager")
@SessionScoped
public class IdentityManager implements Serializable {

    private static final Logger logger = Logger.getLogger(IdentityManager.class.getName());

    private User user; // The JPA entity.
    
    private String nameOrEmail;
    
    private Date lastLogin;

    @Inject
    private UserService userService;
    
    @Inject 
    private ActivityLogger activityLogger;

    public User getUser() {
        if (user == null) {
            Principal principal = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal();
            if (principal != null) {
                user = userService.findByEmail(principal.getName()); // Find User by j_username.
                lastLogin = user.getLastLogin();
                userService.setLastLogin(user);
                
            }
            logger.log(Level.INFO, "{0} logged in.", user);
            activityLogger.log(user.getEmail(), "LOGIN", null);
            nameOrEmail = user.getEmail();
            if(user.getPerson() != null) {
                nameOrEmail = user.getPerson().getName().getFamilyName();
                if (user.getPerson().getName().getFirstName() != null) {
                    nameOrEmail += " " + user.getPerson().getName().getFirstName();
                }
                        
            }
        }
        return user;
    }

    public Date getLastLogin() {
        return lastLogin;
    }
    
    public String getNameOrEmail() {
        if (nameOrEmail == null) {
            getUser();
        }
        return nameOrEmail;
    }
    
    public String logout() {
        if (user != null) {
            logger.log(Level.INFO, "{0} logged out.", user);
            activityLogger.log(user.getEmail(), "LOGOUT", null);
        }
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        user = null;
        return "/all/home?faces-redirect=true";
    }

}
