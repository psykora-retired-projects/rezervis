/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package biz.sykora.rezervis.jsf.backing;

import biz.sykora.rezervis.ejb.UserService;
import biz.sykora.rezervis.model.User;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named("confirmEmail")
@RequestScoped
public class ConfirmEmailView {
    private static final Logger logger = Logger.getLogger(ConfirmEmailView.class.getName());

    @Inject
    private UserService userService; 
    
    private String key;
    private User user;
    private boolean dbError;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public User getUser() {
        return user;
    }
    
    public boolean isConfirmed() {
        return !(user == null);
    }
    
    public boolean isDbError() {
        return dbError;
    }
    
    public void onLoad() {
        try {
            user = userService.confirmUser(key);  // user is null, if not found
            dbError = false;
        } catch (Exception e) {
            user = null;
            dbError = true;
            logger.log(Level.SEVERE, "Confirmation of a user with the verification key {0} failed because of database error.", key);
        }
        // TODO: more exceptions?
    }
    // TODO: login automatically???
    
}
