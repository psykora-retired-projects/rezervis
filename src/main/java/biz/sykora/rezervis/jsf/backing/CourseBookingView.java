/*
 * Copyright (c) 2014-2017, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package biz.sykora.rezervis.jsf.backing;

import biz.sykora.rezervis.cdi.IdentityManager;
import biz.sykora.rezervis.ejb.ActivityLogger;
import biz.sykora.rezervis.ejb.CourseService;
import biz.sykora.rezervis.ejb.MailService;
import biz.sykora.rezervis.ejb.UserService;
import biz.sykora.rezervis.jsf.util.DateUtils;
import biz.sykora.rezervis.jsf.util.StUtils;
import biz.sykora.rezervis.jsf.util.TextUtils;
import biz.sykora.rezervis.jsf.util.WeekDay;
import biz.sykora.rezervis.model.Audience;
import biz.sykora.rezervis.model.CourseReservation;
import biz.sykora.rezervis.model.OfferedCourse;
import biz.sykora.rezervis.model.Participant;
import biz.sykora.rezervis.model.User;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named("courseBooking")
@ViewScoped
public class CourseBookingView implements Serializable {

    private static final String ICON_CHECK = "<i class='fi-check medium'></i>";
    private static final String ICON_ASTERISK = "<i class='fi-asterisk medium'></i>";
    private static final Logger logger = Logger.getLogger(CourseBookingView.class.getName());
    private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
    private static final Set<Audience> CHILD_AUDIENCE = Stream.of(Audience.CHILD, Audience.CHILD_PARENT).collect(Collectors.toSet());


    @Inject
    private IdentityManager identityManager;

    @Inject
    private UserService userService;

    @Inject
    private CourseService courseService;
    
    @Inject
    private MailService mailService;

    @Inject
    ActivityLogger activityLogger;

    private long courseId;

    private OfferedCourse course;

    // who is allowed to register
    private List<Participant> bookingCandidates = new ArrayList<>();

    // who is already registered
    private List<Participant> registrants = new ArrayList<>();

    // who is being registered
    private List<Participant> applicants;

    private Map<Long, Boolean> checked = new HashMap<>();

    private String weekDayStr;
    
    private String message;

    // internal list of newly created reservations
    private final List<CourseReservation> reservations = new LinkedList<>();
    
    public long getCourseId() {
        return courseId;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }

    public OfferedCourse getCourse() {
        return course;
    }

    public void setCourse(OfferedCourse course) {
        this.course = course;
    }

    public String getSanitizedComment() {
        return TextUtils.sanitizeHtml(course.getComment());
    }
    
    public List<Participant> getBookingCandidates() {
        return bookingCandidates;
    }

    public List<Participant> getRegistrants() {
        return registrants;
    }

    public void setRegistrants(List<Participant> registrants) {
        this.registrants = registrants;
    }

    public Map<Long, Boolean> getChecked() {
        return checked;
    }

    public void setChecked(Map<Long, Boolean> checked) {
        this.checked = checked;
    }

    public String getWeekDayStr() {
        return weekDayStr;
    }

    public String getIconCheck() {
        return ICON_CHECK;
    }

    public String getIconAsterisk() {
        return ICON_ASTERISK;
    }

    public String getMessage() {
        return message;
    }
    
    public void init() {
        logger.log(Level.INFO, "Finding course id = {0}", courseId);
        course = courseService.findCourse(courseId);
        weekDayStr = WeekDay.valueOf(course.getWeekDay()).getName();
        User user = userService.find(identityManager.getUser().getId());
        // TODO: what if no personal data / children
        registrants = userService.findRegistrants(user.getPerson(), course);
        Date reservationStart = findReservationStart();
        if (reservationStart != null && reservationStart.after(new Date())) {
            message = "Bohužel si zatím nemůžete kurz rezervovat. Rezervace začínají až "
                    + DateUtils.dateTimeFormat().format(reservationStart)
                    + ". Zkuste to prosím později.";
            return;
        }
        if (course.getCapacity() > 0) {
            bookingCandidates = userService.findBookingCandidates(user.getPerson(), course);
        }
    }

    public String book() {
        applicants = bookingCandidates.stream()
                .filter((c) -> checked.get(c.getId()) != null && checked.get(c.getId()))
                .distinct() // for sure - to avoid booking duplication
                .collect(Collectors.toList());
        FacesContext ctx = FacesContext.getCurrentInstance();
        ctx.getExternalContext().getFlash().setKeepMessages(true);
        String home = "/all/home?faces-redirect=true";
        if (applicants.isEmpty()) {
            ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Rezervace neproběhla, nebyla vybrána žádná osoba.", null));
            logger.log(Level.INFO, "No course reservation because no participant chosen.");
            return home;
        }
        try {
            applicants.forEach( (a) -> reservations.add(courseService.createCourseReservation(course, a)) );
            String resText = reservations.stream().map( (r) -> String.join(" ", r.toString(), r.getParticipant().toString(), r.getOfferedCourse().toString())).collect(Collectors.joining("; "));
            logger.log(Level.INFO, "Reservation(s) created: {0}", resText);
            String recipient = identityManager.getUser().getEmail();
            mailService.send2(recipient , "Rezervace kurzu v Břežánku/VIGVAMu", createBookingConfirmation());
            logger.log(Level.INFO, "Confirmation email sent to {0}", recipient);
        } catch (Exception e) {
            ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Při zpracování Vaší rezervace bohužel došlo k chybě. Zkontrolujte si prosím rezervace a případně se pokuste zadat rezervaci ještě jednou.", null));
            logger.log(Level.WARNING, "Error during course reservation:", e);
            return home;
        }
        ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Vaše rezervace byla uložena.", null));
        return home;
    }
    
    private String createBookingConfirmation() {
        StringBuilder bgn = new StringBuilder();
        if (course.getDateBegin() != null) {
            bgn.append("je ");
            bgn.append(dateFormatter.format(course.getDateBegin()));
        } else {
            bgn.append("bude ještě upřesněn. Sledujte naše webové stránky nebo se zeptejte na recepci");
        }

        StringBuilder list = new StringBuilder();
        reservations.forEach( (r) -> { 
            list.append("\n\t"); 
            list.append(r.getParticipant().getName().getFirstName()); 
            list.append(" "); 
            list.append(r.getParticipant().getName().getFamilyName()); 
            if (r.getRank() < 0) {
                list.append(" - náhradník");
            } else {
                list.append(" - účastník");
            }
        });
        
        Map<String, Object> values = new HashMap<>();
        values.put("participantslist", list.toString());
        values.put("coursename", course.getName());
        values.put("weekday", weekDayStr);
        values.put("timebegin", String.format("%02d:%02d", course.getMinuteBegin() / 60, course.getMinuteBegin() % 60));
        values.put("timeend", String.format("%02d:%02d", course.getMinuteEnd() / 60, course.getMinuteEnd() % 60));
        values.put("datebegintxt", bgn.toString());
        values.put("commenttxt", course.getComment() == null ? "" : "\n\nPoznámka: " + course.getComment());
        String text;
        if (CHILD_AUDIENCE.contains(course.getAudience())) {  // Brezanek
            text = StUtils.renderTemplate("createbookingconfrm", values);
        } else {  // VIGVAM
            text = StUtils.renderTemplate("createbookingconfrm2", values);
        }
        
        return text;
    }
    
    // This is a workaround only. 
    // Returns reservationStart for courses with audience CHILD or CHILD_PARENT,
    // returns reservationStart2 otherwise (i.e. for teenagers, adults and seniors)
    private Date findReservationStart() {
        if (CHILD_AUDIENCE.contains(course.getAudience())) {
            return course.getSeason().getReservationStart();
        } else { // adult audience
            return course.getSeason().getReservationStart2();
        }
    }    
}
