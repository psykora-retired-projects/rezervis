/*
 * Copyright (c) 2014-2017, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package biz.sykora.rezervis.jsf.backing;

import biz.sykora.rezervis.ejb.CourseService;
import biz.sykora.rezervis.ejb.MailService;
import biz.sykora.rezervis.ejb.UserService;
import biz.sykora.rezervis.jsf.util.DateUtils;
import biz.sykora.rezervis.jsf.util.StUtils;
import biz.sykora.rezervis.jsf.util.WeekDay;
import biz.sykora.rezervis.model.Audience;
import biz.sykora.rezervis.model.CourseReservation;
import biz.sykora.rezervis.model.OfferedCourse;
import biz.sykora.rezervis.model.Participant;
import java.io.Serializable;
import java.text.DateFormat;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.Min;

@Named(value = "courseRsvAdd")
@ViewScoped
public class CourseReservAddView implements Serializable {
    private static final Set<Audience> CHILD_AUDIENCE = Stream.of(Audience.CHILD, Audience.CHILD_PARENT).collect(Collectors.toSet());

    private static final Logger logger = Logger.getLogger(CourseReservAddView.class.getName());

    @Inject
    CourseService courseService;

    @Inject
    private UserService userService;
    
    @Inject
    private MailService mailService;

    @Min(value=1, message="Bez výběru nového kurzu to nejde!")
    private long courseId;  // ID of chosen course

    protected Map<String, Long> course2val = new LinkedHashMap<>();  // for course menu

    @Min(value=1, message="Bez výběru účastníka to nejde!")
    private long participantId; // ID od chosen participant
    
    protected Map<String, Long> participant2val = new LinkedHashMap<>();  // for participant menu

    private boolean sendEmail = true;
    
    public Map<String, Long> getCourse2val() {
        return course2val;
    }

    public void setCourse2val(Map<String, Long> course2val) {
        this.course2val = course2val;
    }

    public long getCourseId() {
        return courseId;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }

    public long getParticipantId() {
        return participantId;
    }

    public void setParticipantId(long participantId) {
        this.participantId = participantId;
    }

    public Map<String, Long> getParticipant2val() {
        return participant2val;
    }

    public void setParticipant2val(Map<String, Long> participant2val) {
        this.participant2val = participant2val;
    }

    public boolean isSendEmail() {
        return sendEmail;
    }

    public void setSendEmail(boolean sendEmail) {
        this.sendEmail = sendEmail;
    }

    public String submit() {
        logger.log(Level.INFO, "Creating new reservation course id={0}, participant id={1}", 
                new Object[]{String.valueOf(courseId), String.valueOf(participantId)});
        CourseReservation newRes = courseService.createCourseReservation(courseId, participantId);
        if (newRes == null) {
            FacesContext ctx = FacesContext.getCurrentInstance();
            ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Vytvoření rezervace se nepodařilo. Vybraná osoba rezervaci již má.", null));
            logger.log(Level.WARNING, "Reservation creation failed. The participant is alread present.");
            return null;
        }
        logger.log(Level.INFO, "Reservation {0} created by manager.", newRes);
       
        if (sendEmail) {
            logger.log(Level.INFO, "Sending reservation confirmation email to {0}.", newRes.getParticipant().getPerson().getUser().getEmail());
            // send e-mail
            Map<String, Object> values = new HashMap<>();
            OfferedCourse newCourse = newRes.getOfferedCourse();
            values.put("participtype", newRes.getRank() >= 0 ? "Účastník" : "Náhradník");
            values.put("participname", newRes.getParticipant().getName().getFullName());  // should be the same name
            values.put("coursename", newCourse.getName());
            values.put("weekday", WeekDay.valueOf(newCourse.getWeekDay()).getName());
            values.put("timebegin", String.format("%d:%02d", newCourse.getMinuteBegin() / 60, newCourse.getMinuteBegin() % 60));
            values.put("timeend", String.format("%d:%02d", newCourse.getMinuteEnd() / 60, newCourse.getMinuteEnd() % 60));
            String text;
            if (CHILD_AUDIENCE.contains(newRes.getOfferedCourse().getAudience())) {  // Brezanek
                text = StUtils.renderTemplate("bookingnewconfrm", values);
            } else {  // VIGVAM
                text = StUtils.renderTemplate("bookingnewconfrm2", values);
            }
            mailService.send2(newRes.getParticipant().getPerson().getUser().getEmail(), "Nová rezervace", text);
        } else {
            logger.log(Level.INFO, "New reservation confirmation email not sent - disabled in GUI.");
        }

        return "course-reserv-mgmt?faces-redirect=true";
    }

    @PostConstruct
    public void init() {
        // populate courses menu
        List<OfferedCourse> courses = courseService.findCurrentCourses();
        course2val.put("-- vyberte kurz --", 0L);
        courses.forEach((c) -> {
                String weekDay = WeekDay.valueOf(c.getWeekDay()).getAbbrevCap();
                String timeBeg = String.format(" %02d:%02d", c.getMinuteBegin() / 60, c.getMinuteBegin() % 60);
                String instructor = " (" + c.getInstructor().getName().getFamilyName() + ")";
                String balance = " [" + c.getBalance() + "]";
                course2val.put(weekDay + timeBeg + " " + c.getName() + instructor + balance, c.getId());
        });

        // populate participants menu
        DateFormat df = DateUtils.dateFormat();
        List<Participant> participants = userService.findAllParticipants();
        participant2val.put("-- vyberte účastníka --", 0L);
        participants.forEach((p) -> {
                String born = p.getDateBorn() == null ? "" : " " + df.format(p.getDateBorn());
                String parent = " (" + p.getPerson().getName().getFullName() + ")";
                participant2val.put(p.getName().getFullName() + born + parent, p.getId());
        });
    }

}
