/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package biz.sykora.rezervis.jsf.backing;

import biz.sykora.rezervis.ejb.UserService;
import biz.sykora.rezervis.model.Participant;
import biz.sykora.rezervis.model.User;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named(value = "userMgmt")
@ViewScoped
public class UserManagementView implements Serializable {
    private static final Logger logger = Logger.getLogger(UserManagementView.class.getName());

    @Inject
    private UserService userService; 

    private List<User> users;

    public List<User> getUsers() {
        return users;
    }

    public void deleteUser(User u) {
        logger.log(Level.INFO, "Deleting user {0}.", u.getEmail());
        if (u.getPerson() == null) {
            userService.deleteUser(u);
            users.remove(u);
        } else {
            List<Participant> participants =  u.getPerson().getParticipants();
            boolean noReservations = true;
            for(Participant p : participants) {
                noReservations = noReservations && p.getCourseReservations().isEmpty();
            }
            if (noReservations) {
                // TODO: should we send an informational e-mail?
                userService.deleteUser(u);
                users.remove(u);
            } else {
                // The user cannot be deleted - participants have reservations
                logger.log(Level.WARNING, "User {0} cannot be deleted. She or her children have reservations.", u.getEmail());
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Uživatele nelze vymazat. Nejprve vymažte její rezervace.", null));
            }
        }
    }
    
    @PostConstruct
    public void init() {
        users = userService.getAllUsers(); 
    }
        
}
