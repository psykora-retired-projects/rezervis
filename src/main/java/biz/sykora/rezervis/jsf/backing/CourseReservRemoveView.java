/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package biz.sykora.rezervis.jsf.backing;

import biz.sykora.rezervis.ejb.CourseService;
import biz.sykora.rezervis.ejb.MailService;
import biz.sykora.rezervis.jsf.util.StUtils;
import biz.sykora.rezervis.jsf.util.WeekDay;
import biz.sykora.rezervis.model.CourseReservation;
import biz.sykora.rezervis.model.OfferedCourse;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named(value = "courseRsvRemv")
@ViewScoped
public class CourseReservRemoveView implements Serializable {

    private static final Logger logger = Logger.getLogger(CourseReservRemoveView.class.getName());

    @Inject
    CourseService courseService;

    @Inject
    private MailService mailService;

    private long reservationId;  // original reservation ID, comes via query parameters

    private CourseReservation originalReservation;

    private long substituteId;

    protected Map<String, Long> substitutes = new LinkedHashMap<>();

    public long getReservationId() {
        return reservationId;
    }

    public void setReservationId(long reservationId) {
        this.reservationId = reservationId;
    }

    public CourseReservation getOriginalReservation() {
        return originalReservation;
    }

    public long getSubstituteId() {
        return substituteId;
    }

    public void setSubstituteId(long substituteId) {
        this.substituteId = substituteId;
    }

    public Map<String, Long> getSubstitutes() {
        return substitutes;
    }

    public void setSubstitutes(Map<String, Long> substitutes) {
        this.substitutes = substitutes;
    }

    public String submit() {
        if (originalReservation.getRank() < 0 || substitutes.isEmpty()) {
            // delete the substitute's reservation or delete the participant's one where no substitutes
            deleteReservation();
        } else {
            // delete the participant's reservation and choose a substitute
            deleteAndReplaceReservation();
        }
        return "course-reserv-mgmt?faces-redirect=true";
    }
    
    
    /**
     * Deletes a course reservation and replace it with a chosen reservation of a substitute.
     * 
     * The reservation may be deleted this way only if:
     * <ul>
     *  <li>rank &ge; 0 and there are substitutes</li>
     * </ul>
     * Ranks of other course participants or substitutes are adjusted.
     * A notification e-mail is sent to the related user.
     */
    private void deleteAndReplaceReservation() {
        logger.log(Level.INFO, "Deleting course reservation {0} and replacing it with reservation id={1}",
                new Object[]{originalReservation, String.valueOf(substituteId)});
        OfferedCourse course = originalReservation.getOfferedCourse();
        courseService.replaceAndDeleteReservation(originalReservation.getId(), substituteId);
        logger.log(Level.INFO, "Delete and replace reservation done, sending confirmation emails.");
        
        // send e-mails (original and substituted)
        Map<String, Object> values = new HashMap<>();
        values.put("participtype", "Účastník");
        values.put("participname", originalReservation.getParticipant().getName().getFullName());
        values.put("coursename", course.getName());
        values.put("weekday", WeekDay.valueOf(course.getWeekDay()).getName());
        values.put("timebegin", String.format("%d:%02d", course.getMinuteBegin() / 60, course.getMinuteBegin() % 60));
        String text = StUtils.renderTemplate("deletebookingconfrm", values);
        mailService.send2(originalReservation.getParticipant().getPerson().getUser().getEmail(), "Zrušení rezervace", text);

        CourseReservation substReservation = courseService.findReservation(substituteId);
        values.remove("participtype");
        values.put("participname", substReservation.getParticipant().getName().getFullName());
        values.put("timeend", String.format("%d:%02d", course.getMinuteEnd() / 60, course.getMinuteEnd() % 60));
        text = StUtils.renderTemplate("bookingsubst2partconfrm", values);
        mailService.send2(substReservation.getParticipant().getPerson().getUser().getEmail(), "Změna rezervace", text);
    }

    /**
     * Deletes a course reservation.
     * 
     * The reservation may be deleted this way only if:
     * <ul>
     *  <li>rank &lt; 0</li>
     *  <li>or rank &ge; 0 and there are no substitutes</li>
     * </ul>
     * Ranks of other course participants or substitutes are adjusted.
     * A notification e-mail is sent to the related user.
     */
    private void deleteReservation() {
        logger.log(Level.INFO, "Deleting course reservation {0}.", String.valueOf(originalReservation.getId()));
        courseService.deleteReservation(originalReservation.getId());
        logger.log(Level.INFO, "Delete course reservation done, sending confirmation email.");
        OfferedCourse course = originalReservation.getOfferedCourse();
        Map<String, Object> values = new HashMap<>();
        values.put("participtype", originalReservation.getRank() >= 0 ? "Účastník" : "Náhradník");
        values.put("participname", originalReservation.getParticipant().getName().getFullName());
        values.put("coursename", course.getName());
        values.put("weekday", WeekDay.valueOf(course.getWeekDay()).getName());
        values.put("timebegin", String.format("%d:%02d", course.getMinuteBegin() / 60, course.getMinuteBegin() % 60));
        String text = StUtils.renderTemplate("deletebookingconfrm", values);
        mailService.send2(originalReservation.getParticipant().getPerson().getUser().getEmail(), "Zrušení rezervace", text);
    }
    
    public void init() {
        originalReservation = courseService.findReservation(reservationId);
        List<CourseReservation> reservations = courseService.findSubstReservations(originalReservation.getOfferedCourse().getId());
        if (!reservations.isEmpty()) {
            substituteId = reservations.get(0).getId();
        }
        reservations.forEach((r) -> substitutes.put(r.getParticipant().getName().getFullName(), r.getId()));
    }

}
