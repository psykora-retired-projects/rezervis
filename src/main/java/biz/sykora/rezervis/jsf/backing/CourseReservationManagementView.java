/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package biz.sykora.rezervis.jsf.backing;

import biz.sykora.rezervis.ejb.CourseService;
import biz.sykora.rezervis.jsf.util.DateUtils;
import biz.sykora.rezervis.model.CourseReservation;
import biz.sykora.rezervis.model.OfferedCourse;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.UriBuilder;

@Named(value = "courseRsvMgmt")
@ViewScoped
public class CourseReservationManagementView implements Serializable {
    private static final Logger logger = Logger.getLogger(CourseReservationManagementView.class.getName());
    private static final String RESERVATION_EMAIL_TEMPLATE  
            = "   %s: %s\n"
            + "   Kurz: %s\n"
            + "   Den a čas: %d / %d:%02d\n";

    @Inject
    private CourseService courseService;
    
    private List<CourseReservation> reservations;

    public List<CourseReservation> getReservations() {
        return reservations;
    }
    
    /**
     * Creates a link for opening a new GMail windows with pre-selected
     * sender, subject and body fragment.
     * 
     * Warning: This is a hack, the approach seems to work, 
     * but it is not documented by Google.
     * 
     * @param r current reservation
     * @return the URL string
     */
    public String gmailUrl(CourseReservation r) {
        String email = r.getParticipant().getPerson().getUser().getEmail();
        OfferedCourse course = r.getOfferedCourse();
        String msg = "Dobrý den,\n\n\n" + String.format(RESERVATION_EMAIL_TEMPLATE,
                r.getRank() >= 0 ? "Účastník" : "Náhradník",
                r.getParticipant().getName().getFullName(),
                course.getName(), course.getWeekDay(), course.getMinuteBegin() / 60, course.getMinuteBegin() % 60);
        return UriBuilder.fromUri("https://mail.google.com/mail/")
                .queryParam("view", "cm")
                .queryParam("fs", "1")
                .queryParam("tf", "1")
                .queryParam("to", email)
                .queryParam("su", "Vaše rezervace ze dne " + DateUtils.dateFormat().format(r.getReservationTime()))
                .queryParam("body", msg)
                .build().toString();
    }
    
    @PostConstruct
    public void init() {
        reservations = courseService.findCurrentReservations();
    }
    
}
