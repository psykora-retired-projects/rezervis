/*
 * Copyright (c) 2014-2017, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package biz.sykora.rezervis.jsf.backing;

import biz.sykora.rezervis.ejb.CourseService;
import biz.sykora.rezervis.ejb.MailService;
import biz.sykora.rezervis.jsf.util.StUtils;
import biz.sykora.rezervis.jsf.util.WeekDay;
import biz.sykora.rezervis.model.CourseReservation;
import biz.sykora.rezervis.model.OfferedCourse;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named(value = "courseDel")
@ViewScoped
public class CourseDeleteView implements Serializable {

    private static final Logger logger = Logger.getLogger(CourseDeleteView.class.getName());

    @Inject
    CourseService courseService;

    @Inject
    private MailService mailService;

    private long courseId;  // ID of the course to delete (comes via query parameters)

    private OfferedCourse course;  // course to delete
    
    private List<CourseReservation> reservations;
    
    private boolean sendEmail = true;
    
    public long getCourseId() {
        return courseId;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }

    public OfferedCourse getCourse() {
        return course;
    }

    public void setCourse(OfferedCourse course) {
        this.course = course;
    }

    public boolean isSendEmail() {
        return sendEmail;
    }

    public void setSendEmail(boolean sendEmail) {
        this.sendEmail = sendEmail;
    }
    
    public String submit() {
        logger.log(Level.INFO, "Deleting {0}.", course.toString());
        
        // delete all the reservation (in reverse order - substitutes must be deleted first, then participants)
        Collections.reverse(reservations);
        reservations.forEach(this::deleteReservation);
        reservations.clear();
        
        // delete the course
        courseService.deleteCourse(course.getId());
        logger.log(Level.INFO, "{0} and its reservations were deleted.", course.toString());
        
        return "course-mgmt?faces-redirect=true";
    }
    
    
    /**
     * Deletes a particular course reservation.
     * 
     * The reservation may be deleted this way only if:
     * <ul>
     *  <li>rank &lt; 0</li>
     *  <li>or rank &ge; 0 and there are no substitutes</li>
     * </ul>
     * Ranks of other course participants or substitutes are adjusted, if needed.
     * A notification e-mail is sent to the related user.
     * 
     * Attention: For smooth deleting of several reservations, it is necessary to delete the reservations from the smallest rank.
     */
    private void deleteReservation(CourseReservation reservationToDelete) {
        logger.log(Level.INFO, "Deleting course reservation {0}.", String.valueOf(reservationToDelete.getId()));
        courseService.deleteReservation(reservationToDelete.getId());
        if (sendEmail) {
            logger.log(Level.INFO, "Delete course reservation done, sending confirmation emails.");
            Map<String, Object> values = new HashMap<>();
            values.put("participtype", reservationToDelete.getRank() >= 0 ? "Účastník" : "Náhradník");
            values.put("participname", reservationToDelete.getParticipant().getName().getFullName());
            values.put("coursename", course.getName());
            values.put("weekday", WeekDay.valueOf(course.getWeekDay()).getName());
            values.put("timebegin", String.format("%d:%02d", course.getMinuteBegin() / 60, course.getMinuteBegin() % 60));
            String text = StUtils.renderTemplate("deletebookingconfrm", values);
            mailService.send2(reservationToDelete.getParticipant().getPerson().getUser().getEmail(), "Zrušení rezervace", text);
        } else {
            logger.log(Level.INFO, "Delete course reservation done, no confirmation emails sent.");
        }
    }
    
    public void init() {
        course = courseService.findCourse(courseId);
        reservations = courseService.findCourseReservations(courseId);
    }

}
