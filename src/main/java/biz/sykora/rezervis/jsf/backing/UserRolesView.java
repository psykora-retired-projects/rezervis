/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package biz.sykora.rezervis.jsf.backing;

import biz.sykora.rezervis.ejb.UserService;
import biz.sykora.rezervis.model.Role;
import biz.sykora.rezervis.model.User;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named(value = "userRoles")
@ViewScoped
public class UserRolesView implements Serializable {
    private static final Logger logger = Logger.getLogger(UserRolesView.class.getName());

    @Inject
    private UserService userService; 

    private List<User> users;

    private Map<User, String[]> selectedRoles = new HashMap<>();

    private final List<String> availableRoles = Arrays.asList("USER", "OPERATOR", "MANAGER", "ADMIN");

    public List<User> getUsers() {
        return users;
    }

//    public List<String> selectedRoles(User u) {
//        return u.getRoles().stream().map(Role::name).collect(Collectors.toList());
//    }
//
//    public void selectedRoles(User u, List<String> roles) {
//    }

    
    public Map<User,String[]> getSelectedRoles() {
        return selectedRoles;
    }

    public void setSelectedRoles(Map<User,String[]> selectedRoles) {
        this.selectedRoles = selectedRoles;
    }

    public List<String> getAvailableRoles() {
        return availableRoles;
    }
    
    public String save(User u) {
        logger.log(Level.INFO, "Updated roles for user {0}.", u.getEmail());
        userService.updateUserRoles(u, Arrays.asList(selectedRoles.get(u)));
        return null;
    }
    
    @PostConstruct
    public void init() {
        users = userService.getAllUsers();
        users.forEach( (u) -> {
            List<String> roleList = u.getRoles().stream().map(Role::name).collect(Collectors.toList());
            selectedRoles.put(u, roleList.toArray(new String[roleList.size()]));
        });
    }
        
}
