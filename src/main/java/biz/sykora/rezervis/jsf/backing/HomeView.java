/*
 * Copyright (c) 2014-2016, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package biz.sykora.rezervis.jsf.backing;

import biz.sykora.rezervis.ejb.UserService;
import biz.sykora.rezervis.cdi.IdentityManager;
import biz.sykora.rezervis.ejb.CourseService;
import biz.sykora.rezervis.jsf.util.WeekDay;
import biz.sykora.rezervis.model.Membership;
import biz.sykora.rezervis.model.Participant;
import biz.sykora.rezervis.model.Person;
import biz.sykora.rezervis.model.User;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named(value = "home")
@RequestScoped
public class HomeView {
    private static final Logger logger = Logger.getLogger(HomeView.class.getName());
    
    @Inject
    private IdentityManager identityManager;
    
    @Inject
    private UserService userService;
    
    @Inject
    private CourseService courseService;
    
    private User user;
    private Person person;
    private List<Participant> participants = new ArrayList<>(); 
    private List<Participant> children = new ArrayList<>(); 
    private int numberOfNewReservations = 0;
    private Date lastLogin;

    public User getUser() {
        return user;
    }
    
    public boolean isEmailConfirmed() {
        return user.isVerified();
    }

    public Person getPerson() {
        return person;
    }

    public List<Participant> getParticipants() {
        return participants;
    }
    
    public List<Participant> getChildren() {
        return children;
    }

    public int getNumberOfNewReservations() {
        return numberOfNewReservations;
    }

    public String weekDayAbbrev(int weekDay) {
        if (weekDay < 1 || weekDay > 7) {
            return "";
        }
        return WeekDay.valueOf(weekDay).getAbbrevCap();
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    @PostConstruct
    public void init() {
        this.user = userService.getRefreshedUser(identityManager.getUser());
        this.person = user.getPerson();
        if (person != null) {
            this.participants.addAll(person.getParticipants());
            this.children.addAll(userService.getChildren(person));
        }
        lastLogin = identityManager.getLastLogin();
        if (user.isInRole("MANAGER")) {
            Date since = lastLogin;
            if (since == null) {
                LocalDateTime weekAgo = LocalDateTime.now().minusWeeks(1);
                since = Date.from(weekAgo.toInstant(ZoneOffset.UTC));
            }
            numberOfNewReservations = courseService.findNumberOfNewReservations(since);
        }
    }

}
