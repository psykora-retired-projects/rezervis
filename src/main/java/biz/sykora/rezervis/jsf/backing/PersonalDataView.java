/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package biz.sykora.rezervis.jsf.backing;

import biz.sykora.rezervis.ejb.UserService;
import biz.sykora.rezervis.cdi.IdentityManager;
import biz.sykora.rezervis.model.Address;
import biz.sykora.rezervis.model.Gender;
import biz.sykora.rezervis.model.Name;
import biz.sykora.rezervis.model.Person;
import biz.sykora.rezervis.model.User;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Named(value = "personalData")
@RequestScoped
public class PersonalDataView {
    private static final Logger logger = Logger.getLogger(PersonalDataView.class.getName());

    @Inject
    private UserService userService; 
    
    @Inject
    private IdentityManager identityManager;

    private long userId;
    
    private String title;
    
    @NotNull(message = "Jméno musí být zadáno!")
    @Size(min = 2, max = 32, message = "Délka jména musí být 2 až 32 znaků.")
    private String firstName;
    
    @NotNull(message = "Příjmení musí být zadáno!")
    @Size(min = 2, max = 32, message = "Délka příjmení musí být 2 až 32 znaků.")
    private String familyName;
    
    @NotNull(message = "Ulice (část obce) musí být zadána!")
    @Size(min = 2, max = 64, message = "Název ulice (části obce) s číslem musí být dlouhý 2 až 32 znaků.")
    private String street;
    
    @NotNull(message = "Město (obec) musí být zadáno!")
    @Size(min = 2, max = 64, message = "Název města nebo obce musí být dlouhý 2 až 64 znaků.")
    private String city;
    
    @Size(min = 2, max = 16, message = "Délka PSČ musí být 2 až 16 znaků.")
    private String postcode;
    
    @NotNull(message = "Toto telefonní číslo musí být zadáno!")
    @Size(min = 9, max = 16, message = "Telefonní číslo musí být dlouhé 9 až 16 znaků.")
    @Pattern(regexp="^\\+?[\\d ]{9,15}$", message = "Telefonní číslo může obsahovat pouze číslice a mezery (případně úvodní znak '+').")
    private String phone;
    
    @Size(min = 9, max = 16, message = "Telefonní číslo musí být dlouhé 9 až 16 znaků.")
    @Pattern(regexp="^\\+?[\\d ]{9,15}$", message = "Telefonní číslo může obsahovat pouze číslice a mezery (případně úvodní znak '+').")
    private String phone2;
    
    @Size(min = 4, max = 4, message = "Rok narození musí mít 4 číslice.")
    @Pattern(regexp="^\\d{4}$", message = "Rok narození může obsahovat pouze číslice.")
    private String yearBornStr;
    
    public PersonalDataView() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String lastName) {
        this.familyName = lastName;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode.replace(" ", "");
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getYearBornStr() {
        return yearBornStr;
    }

    public void setYearBornStr(String yearBornStr) {
        this.yearBornStr = yearBornStr;
    }
    
    public String save() {
        Name name = new Name(firstName, familyName);
        Gender gender = Gender.valueOf(title);
        Address address = new Address(street, city, postcode);
        userService.addOrUpdatePerson(userId, name, address, phone, phone2, gender, yearBornStr);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.getExternalContext().getFlash().setKeepMessages(true);
        fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Vaše osobní údaje byly uloženy do databáze.", null));
        return "/all/home?faces-redirect=true";
    }
    
    @PostConstruct
    public void init() {
        userId= identityManager.getUser().getId();
        User u = userService.find(userId);
        Person p = u.getPerson();
        if (p != null) {
            title = p.getGender().toString();
            firstName = p.getName().getFirstName();
            familyName = p.getName().getFamilyName();
            street = p.getAddress().getStreet();
            city = p.getAddress().getCity();
            postcode = p.getAddress().getPostcode();
            phone = p.getPhone();
            phone2 = p.getPhone2();
            yearBornStr = p.getYearBorn() == 0 ? null : Integer.toString(p.getYearBorn());
        }
    }
        
}
