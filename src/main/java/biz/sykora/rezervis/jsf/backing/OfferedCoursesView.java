/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package biz.sykora.rezervis.jsf.backing;

import biz.sykora.rezervis.ejb.CourseService;
import biz.sykora.rezervis.jsf.util.WeekDay;
import biz.sykora.rezervis.model.Audience;
import biz.sykora.rezervis.model.OfferedCourse;
import biz.sykora.rezervis.model.Season;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named("offeredCourses")
@RequestScoped
public class OfferedCoursesView {
    private static final Logger logger = Logger.getLogger(OfferedCoursesView.class.getName());
    
    @Inject
    private CourseService courseService; 
    
    private Season season;

    private String audienceStr;
    
    private List<OfferedCourse> courses = new ArrayList<>();

    public String getAudienceStr() {
        return audienceStr;
    }

    public void setAudienceStr(String audienceStr) {
        this.audienceStr = audienceStr;
    }

    public List<OfferedCourse> getCourses() {
        return courses;
    }

    public List<OfferedCourse> getDayCourses(int weekDay) {
        // TODO: check weekDay is between 1 and 7
        return courses.stream().filter((c) -> c.getWeekDay() == weekDay).collect(Collectors.toList());
    }

    public void setCourses(List<OfferedCourse> courses) {
        this.courses = courses;
    }
    
    public List<WeekDay> getWeekDays() {
        return WeekDay.getWeekDays();
    }
    
    public Season getSeason() {
        return season;
    }

    public void setSeason(Season season) {
        this.season = season;
    }
    
    public void init() {
        logger.log(Level.INFO, "Finding courses for {0}", audienceStr);
        courses = courseService.findOfferedCourses(Audience.valueOf(audienceStr));
        if (courses.isEmpty()) {
            season = courseService.findCurrentSeason();
        } else {
            season = courses.get(0).getSeason();
        }
    }

}
