
/*
 * Copyright (c) 2014-2017, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package biz.sykora.rezervis.jsf.backing;

import biz.sykora.rezervis.ejb.CourseService;
import biz.sykora.rezervis.model.OfferedCourse;
import biz.sykora.rezervis.model.Season;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Named(value = "seasonMgmt")
@RequestScoped
public class SeasonManagementView {

    private static final Logger logger = Logger.getLogger(SeasonManagementView.class.getName());

    @Inject
    private CourseService courseService;
    
    private Date reservationStart;
    private Date reservationStart2;
    
    @NotNull(message = "Tento údaj potřebujeme!")
    private Date dateBegin;
    
    @NotNull(message = "Tento údaj potřebujeme!")
    private Date dateEnd;

    @NotNull(message = "Období musí mít jméno (např.: 1. pololetí 2014/2015)!")
    private String name;

    @NotNull(message = "Tento údaj potřebujeme!")
    @Min(value = 2000, message = "Rok musí být větší nebo roven 2000!")
    @Max(value = 2199, message = "Rok musí být menší než 2200!")
    private int yearPrev;
    
    @NotNull(message = "Tento údaj potřebujeme!")
    private String namePrev;
    
    private List<OfferedCourse> expiredCourses;

    public Date getReservationStart() {
        return reservationStart;
    }

    public void setReservationStart(Date reservationStart) {
        this.reservationStart = reservationStart;
    }

    public Date getReservationStart2() {
        return reservationStart2;
    }

    public void setReservationStart2(Date reservationStart2) {
        this.reservationStart2 = reservationStart2;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYearPrev() {
        return yearPrev;
    }

    public void setYearPrev(int yearPrev) {
        this.yearPrev = yearPrev;
    }

    public String getNamePrev() {
        return namePrev;
    }

    public void setNamePrev(String namePrev) {
        this.namePrev = namePrev;
    }

    public List<OfferedCourse> getExpiredCourses() {
        return expiredCourses;
    }
    
    
    public String saveReservationStart() {
        Season season = courseService.findCurrentSeason();
        season.setReservationStart(reservationStart);
        season.setReservationStart2(reservationStart2);
        logger.log(Level.INFO, "Saving reservation starts of season {0}.", season.getId());
        courseService.updateSeason(season);
        return null;
    }

    public String save() {
        Season season = courseService.findCurrentSeason();
        season.setName(name);
        season.setDateBegin(dateBegin);
        season.setDateEnd(dateEnd);
        logger.log(Level.INFO, "Updating season {0}.", season.getId());
        courseService.updateSeason(season);
        courseService.updatePreviousSeason(yearPrev, namePrev);
        // reload the current page to refresh the expired courses
        String viewId = FacesContext.getCurrentInstance().getViewRoot().getViewId();
        return viewId + "?faces-redirect=true";
    }

    @PostConstruct
    public void init() {
        Season season = courseService.findCurrentSeason();
        reservationStart = season.getReservationStart();
        reservationStart2 = season.getReservationStart2();
        name = season.getName();
        dateBegin = season.getDateBegin();
        dateEnd = season.getDateEnd();
        Season seasonPrev = courseService.findPreviousSeason();
        namePrev = seasonPrev.getName();
        yearPrev = 1970 + (int) (seasonPrev.getDateBegin().getTime() / (365L * 24L * 60L * 60L * 1000L));
        expiredCourses = courseService.findExpiredCourses();
    }

}
