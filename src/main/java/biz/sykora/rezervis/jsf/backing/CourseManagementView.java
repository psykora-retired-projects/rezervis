/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package biz.sykora.rezervis.jsf.backing;

import biz.sykora.rezervis.ejb.CourseService;
import biz.sykora.rezervis.ejb.UserService;
import biz.sykora.rezervis.model.OfferedCourse;
import biz.sykora.rezervis.model.Role;
import biz.sykora.rezervis.model.User;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named(value = "courseMgmt")
@ViewScoped
public class CourseManagementView implements Serializable {
    private static final Logger logger = Logger.getLogger(CourseManagementView.class.getName());

    @Inject
    private CourseService courseService; 

    private List<OfferedCourse> courses = new ArrayList<>();
    
    private int currentCourseIndex;
    private OfferedCourse editedCourse;
    private int page = 1;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
    
    public List<OfferedCourse> getCourses() {
        return courses;
    }
    
    public String save(OfferedCourse c) {
        logger.log(Level.INFO, "Updating course {0}.", c.getName());
        //courseService.;
        return null;
    }

    public int getCurrentCourseIndex() {
        return currentCourseIndex;
    }

    public void setCurrentCourseIndex(int currentCourseIndex) {
        this.currentCourseIndex = currentCourseIndex;
    }

    public OfferedCourse getEditedCourse() {
        return editedCourse;
    }

    public void setEditedCourse(OfferedCourse editedCourse) {
        this.editedCourse = editedCourse;
    }
    
    public void store() {
        logger.log(Level.INFO, "Store");
        // TODO
    }
    
    @PostConstruct
    public void init() {
        courses = courseService.getAllCourses(); // TODO: season
        logger.log(Level.FINE, "Courses init: {0}", courses.toString());
    }
        
}
