/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package biz.sykora.rezervis.jsf.backing;

import biz.sykora.rezervis.ejb.ActivityLogger;
import biz.sykora.rezervis.ejb.MailService;
import biz.sykora.rezervis.ejb.PaymentService;
import biz.sykora.rezervis.ejb.UserService;
import biz.sykora.rezervis.jsf.util.DateUtils;
import biz.sykora.rezervis.jsf.util.StUtils;
import biz.sykora.rezervis.model.CourseReservation;
import biz.sykora.rezervis.model.Membership;
import biz.sykora.rezervis.model.Participant;
import biz.sykora.rezervis.model.PaymentType;
import biz.sykora.rezervis.model.User;
import java.io.Serializable;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

@Named("addPayment")
@ViewScoped
public class AddPaymentView implements Serializable {
    private static final Logger logger = Logger.getLogger(AddPaymentView.class.getName());
    
    @Inject
    private UserService userService;

    @Inject
    private PaymentService paymentService;
    
    @Inject
    private MailService mailService;

    @Inject
    ActivityLogger activityLogger;

    private long userId;
    
    private User user;
    
    private List<Participant> participants = new ArrayList<>();
    
    private List<CourseReservation> reservations = new ArrayList<>(); 

    private Map<Long, Boolean> checked = new HashMap<>();
    
    private List<String> membership = Collections.EMPTY_LIST;
    
    private List<String> seasonValues = new ArrayList<>();
    
    private String[] seasonsChecked;
    
    private String paymentType;
    
    @NotNull(message = "Datum platby musí být zadáno!")
    @Past(message = "Datum platby nesmí být v budoucnosti.")
    private Date paymentDate;
    
    @NotNull(message = "Částka platby musí být zadána!")
    @Min(value=1, message = "Částka platby musí být větší než nula!")
    private int amount;
    
    private boolean sendEmail = true;
    
    private String comment;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Participant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }

    public List<CourseReservation> getReservations() {
        return reservations;
    }

    public Map<Long, Boolean> getChecked() {
        return checked;
    }

    public void setChecked(Map<Long, Boolean> checked) {
        this.checked = checked;
    }

    public List<String> getMembership() {
        return membership;
    }

    public void setMembership(List<String> membership) {
        this.membership = membership;
    }

    public List<String> getSeasonValues() {
        return seasonValues;
    }

    public void setSeasonValues(List<String> seasonValues) {
        this.seasonValues = seasonValues;
    }

    public String[] getSeasonsChecked() {
        return seasonsChecked;
    }

    public void setSeasonsChecked(String[] seasonsChecked) {
        this.seasonsChecked = seasonsChecked;
    }

    public void setReservations(List<CourseReservation> reservations) {
        this.reservations = reservations;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public boolean isSendEmail() {
        return sendEmail;
    }

    public void setSendEmail(boolean sendEmail) {
        this.sendEmail = sendEmail;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void init() {
        logger.log(Level.INFO, "Finding user id = {0}", userId);
        user = userService.find(userId);
        participants = user.getPerson().getParticipants();
        // find all the reservations
        participants.forEach( (p) -> reservations.addAll(p.getCourseReservations()) );
        // find membership and prepare seasons
        membership = user.getPerson().getMembership().stream().map(Membership::getSeason).collect(Collectors.toList());
        int year = Calendar.getInstance().get(Calendar.YEAR);
        addSeasonValue(year - 1, "/1");
        addSeasonValue(year - 1, "/2");
        addSeasonValue(year, "/1");
        addSeasonValue(year, "/2");
        addSeasonValue(year + 1, "/1");
        addSeasonValue(year + 1, "/2");
    }

    // adds only a season, where no membership is
    private void addSeasonValue(int year, String halfYear) {
        String season = String.valueOf(year) + halfYear;
        if (!membership.contains(season)) {
            seasonValues.add(season);
        }
    } 
    
    public String submit() {
        logger.log(Level.INFO, "Adding payment");
        // process rezervations
        List<CourseReservation> checkedReservations = reservations.stream()
                .filter((r) -> (checked.get(r.getId()) != null && checked.get(r.getId())))
                .collect(Collectors.toList());
        checked.clear(); // If necessary.
        // process membership seasons
        List<String> checkedSeasons = Arrays.asList(seasonsChecked);
        paymentService.createPayment(PaymentType.valueOf(paymentType), paymentDate, amount, user.getPerson(), comment, checkedReservations, checkedSeasons);
        // send the confirmation e-mail
        DateFormat df = DateUtils.dateFormat();
        Map<String, Object> values = new HashMap<>();
        values.put("amount", amount);
        values.put("date", df.format(paymentDate));
        String text = StUtils.renderTemplate("paymconfrm", values);
        if (sendEmail) {
            logger.log(Level.INFO, "Sending payment confirmation email to {0} (amount:{1}, date:{2}).", new Object[]{user.getEmail(), String.valueOf(amount), values.get("date")});
            mailService.send2(user.getEmail(), "Potvrzení platby", text);
        } else {
            logger.log(Level.INFO, "Payment confirmation email not sent - disabled in GUI.");
        }
        return "user-mgmt?faces-redirect=true";
    }
    
}
