/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package biz.sykora.rezervis.jsf.backing;

import biz.sykora.rezervis.ejb.MailService;
import biz.sykora.rezervis.ejb.UserService;
import biz.sykora.rezervis.cdi.IdentityManager;
import biz.sykora.rezervis.jsf.util.StUtils;
import biz.sykora.rezervis.model.User;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Null;

@Named(value = "forgottenPassword")
@RequestScoped
public class ForgottenPasswordView {
    private static final Logger logger = Logger.getLogger(ForgottenPasswordView.class.getName());
    private static final Pattern emailPattern = Pattern.compile("([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)"); 
    private static final String PWD_CHARS_1 = "BbCcDdFfGgHhJjKkLMmNnPpRrSsTtVvWwZz";
    private static final String PWD_CHARS_2 = "aeouiAEU4";
    
    @Inject
    private UserService userService; 
    
    @Inject
    private MailService mailService;
    
    @Inject
    private IdentityManager identityManager;

    private String email;
    
    @Null(message = "Toto pole musí zůstat prázdné!")
    private String honeypot;
    
    public ForgottenPasswordView() {
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHoneypot() {
        return honeypot;
    }

    public void setHoneypot(String honeypot) {
        this.honeypot = honeypot;
    }
    
    public String sendPassword() {
        identityManager.logout();
        processNewPassword();
        return "forgotten-password-ok";
    }
    
    public void validateEmail(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            ((UIInput)component).setValid(false);
            FacesMessage msg = new FacesMessage("E-mailová adresa musí být zadána!");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            context.addMessage(component.getClientId(context), msg);
            return;
        }
        String myEmail = value.toString();
        Matcher m = emailPattern.matcher(myEmail);
        if (!m.matches()) {
            ((UIInput)component).setValid(false);
            FacesMessage msg = new FacesMessage("Nesprávný formát e-mailové adresy!");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            context.addMessage(component.getClientId(context), msg);
        } else {
            User u = userService.find(myEmail);
            if (u == null) {
                ((UIInput)component).setValid(false);
                FacesMessage msg = new FacesMessage("Tato e-mailová adresa není zatím registrována!");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                context.addMessage(component.getClientId(context), msg);
            } else if (u.getVerificationKey() != null) {
                ((UIInput)component).setValid(false);
                FacesMessage msg = new FacesMessage("Tato e-mailová adresa nebyla dosud ověřena! Kontaktuje prosím recepci.");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                context.addMessage(component.getClientId(context), msg);
            }
        }
    }
    
    private void processNewPassword() {        
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest)fc.getExternalContext().getRequest();
        logger.log(Level.INFO, "Sending new password for e-mail {0}.", email);
        try {
            String newPassword = generateRandomPassword();
            String path = req.getContextPath() + "/all/change-password.xhtml";
            URI uri = new URI(req.getScheme(), null, req.getServerName(), req.getServerName().equals("localhost") ? req.getServerPort() : -1, path, null, null);
            userService.updatePassword(email, newPassword);
            Map<String, Object> values = new HashMap<>();
            values.put("newpwd", newPassword);
            values.put("link", uri.toASCIIString());
            String text = StUtils.renderTemplate("forgottenpwd", values);
            mailService.send2(email, "Zapomenuté heslo", text);
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Nové heslo zasláno na adresu " + email, null));
        } catch (URISyntaxException | RuntimeException ex) {
            logger.log(Level.SEVERE, null, ex);
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Chyba při zasílání nového hesla! Kontaktujte prosím recepci.", null));
        }            
    } 
    
    private String generateRandomPassword() {
        StringBuilder buf = new StringBuilder();
        for (int i = 1; i <= 5; ++i) {
            if (i % 3 == 0) {
                buf.append(Math.random() < 0.5 ? '-' : '.');
            }
            buf.append(PWD_CHARS_1.charAt((int)(Math.random() * PWD_CHARS_1.length())));
            buf.append(PWD_CHARS_2.charAt((int)(Math.random() * PWD_CHARS_2.length())));
        }      
        return buf.toString();
    }
    
}
