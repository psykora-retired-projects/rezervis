/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package biz.sykora.rezervis.jsf.backing;

import biz.sykora.rezervis.ejb.CourseService;
import biz.sykora.rezervis.ejb.MailService;
import biz.sykora.rezervis.jsf.util.StUtils;
import biz.sykora.rezervis.jsf.util.WeekDay;
import biz.sykora.rezervis.model.Audience;
import biz.sykora.rezervis.model.CourseReservation;
import biz.sykora.rezervis.model.Instructor;
import biz.sykora.rezervis.model.OfferedCourse;
import biz.sykora.rezervis.model.Participant;
import biz.sykora.rezervis.model.Season;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named(value = "courseEdit")
@ViewScoped
public class CourseEditView implements Serializable {
    private static final Set<Audience> CHILD_AUDIENCE = Stream.of(Audience.CHILD, Audience.CHILD_PARENT).collect(Collectors.toSet());

    private static final Logger logger = Logger.getLogger(CourseEditView.class.getName());

    @Inject
    CourseService courseService;

    @Inject
    private MailService mailService;
    
    private long courseId;  // ID of the course to edit (comes via query parameters), 0 for new course creation
    
    private String xfer; // a flag, if it is a simple course edit or a transfer to the updated season

    private OfferedCourse course;  // course to edit or new course
    
    private String audienceStr;
    
    // TODO: validation
    private short capacity; // the capacity must be processed separately - it might induce balance and reservation changes, emails etc. 
    
    private final Map<String, Long> instructor2value = new LinkedHashMap<>();
    
    private long instructorId;
    
    private List<Participant> substitutes = new ArrayList<>();

    private Map<Long, Boolean> checked = new LinkedHashMap<>();
    
    // For course transfer to the updated season
    private List<CourseReservation> reservations;

    private boolean sendEmail = true;

    public long getCourseId() {
        return courseId;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }

    public String getXfer() {
        return xfer;
    }

    public void setXfer(String xfer) {
        this.xfer = xfer;
    }

    public OfferedCourse getCourse() {
        return course;
    }

    public void setCourse(OfferedCourse course) {
        this.course = course;
    }

    public String getAudienceStr() {
        return audienceStr;
    }

    public void setAudienceStr(String audienceStr) {
        this.audienceStr = audienceStr;
    }

    public Map<String, Long> getInstructor2value() {
        return instructor2value;
    }

    public long getInstructorId() {
        return instructorId;
    }

    public void setInstructorId(long instructorId) {
        this.instructorId = instructorId;
    }

    public short getCapacity() {
        return capacity;
    }

    public void setCapacity(short capacity) {
        this.capacity = capacity;
    }

    public List<Participant> getSubstitutes() {
        return substitutes;
    }

    public Map<Long, Boolean> getChecked() {
        return checked;
    }

    public List<CourseReservation> getReservations() {
        return reservations;
    }

    public boolean isSendEmail() {
        return sendEmail;
    }

    public void setSendEmail(boolean sendEmail) {
        this.sendEmail = sendEmail;
    }

    // Submit simple course edit form
    public String submit() {
        Audience audience = Audience.valueOf(audienceStr);
        course.setAudience(audience);
        
        if (course.getId() == null) {
            // creating new course
            course.setCapacity(capacity);
            course.setBalance(capacity);
            courseService.createCourse(course, instructorId);
            logger.log(Level.INFO, "Created {0}", course.toString());
        } else {
            // create a set of the chosen substitutes
            Set<Long> substIds = checked.entrySet().stream()
                    .filter( (e) -> e.getValue() )
                    .map( (e) -> e.getKey() )
                    .collect(Collectors.toCollection(LinkedHashSet::new));
            // updating existing course
            courseService.updateCourse(course, instructorId, capacity, substIds);
            // TODO: send emails about changes
            logger.log(Level.INFO, "Updated {0}", course.toString());
        }
        
        return "course-mgmt?faces-redirect=true";
    }
    
    // Submit updated season course transfer form
    public String submitTransfer() {
        Audience audience = Audience.valueOf(audienceStr);
        course.setAudience(audience);
        
        if (xfer != null && course.getId() != null) {
            // create a set of preserved reservation IDs
            Set<Long> preservedIds = checked.entrySet().stream()
                    .filter( (e) -> e.getValue() )
                    .map( (e) -> e.getKey() )
                    .collect(Collectors.toCollection(LinkedHashSet::new));
            // create a set of abandoned reservation IDs
            Set<Long> abandonedIds = checked.entrySet().stream()
                    .filter( (e) -> !e.getValue() )
                    .map( (e) -> e.getKey() )
                    .collect(Collectors.toCollection(LinkedHashSet::new));
            // transfer the course
            courseService.transferCourseToSeason(course, instructorId, preservedIds, abandonedIds);
            if (sendEmail) {
                sendConfirmationEmailsXfer();
            }
            logger.log(Level.INFO, "Course {0} transfered to the updated season", course.toString());
        }
        
        return "season-mgmt?faces-redirect=true";
    }
    
    // This initialization is common for both simple course editing and course transfering to the updated season
    public void init() {
        Season season = courseService.findCurrentSeason();
        if (courseId != 0) {
            course = courseService.findCourse(courseId);
            audienceStr = course.getAudience().name();
            instructorId = course.getInstructor().getId();
            capacity = course.getCapacity();
            List<CourseReservation> substReservations = courseService.findSubstReservations(courseId);
            substReservations.forEach((r) -> substitutes.add(r.getParticipant()));
        } else {
            course = new OfferedCourse();
            course.setSeason(season);
            audienceStr = "CHILD";
        }
        
        List<Instructor> instructors = courseService.findInstructors();
        instructors.forEach(this::populateInstructor2Value);
        
        // the following is needed only for a course transfer to the updated season
        if (courseId != 0 && xfer != null) {
            course.setDateBegin(season.getDateBegin());
            course.setDateEnd(season.getDateEnd());
            reservations = courseService.findCourseReservations(courseId);
            reservations.removeIf( (r) -> r.getRank() < 0 );  // remove substitutes
        }
    }

    private void populateInstructor2Value(Instructor i) {
        instructor2value.put(i.getName().getFullName(), i.getId());
    }
    
    private void sendConfirmationEmailsXfer() {
        // for sure, re-read current reservations
        courseService.findCourseReservations(course.getId()).forEach( res -> {
            logger.log(Level.INFO, "Sending reservation confirmation email to {0}.", res.getParticipant().getPerson().getUser().getEmail());
            // send e-mail
            Map<String, Object> values = new HashMap<>();
            OfferedCourse newCourse = res.getOfferedCourse();
            values.put("participtype", res.getRank() >= 0 ? "Účastník" : "Náhradník");
            values.put("participname", res.getParticipant().getName().getFullName());  // should be the same name
            values.put("coursename", newCourse.getName());
            values.put("weekday", WeekDay.valueOf(newCourse.getWeekDay()).getName());
            values.put("timebegin", String.format("%d:%02d", newCourse.getMinuteBegin() / 60, newCourse.getMinuteBegin() % 60));
            values.put("timeend", String.format("%d:%02d", newCourse.getMinuteEnd() / 60, newCourse.getMinuteEnd() % 60));
            //String text = StUtils.renderTemplate("bookingnewconfrm", values);
            String text;
            if (CHILD_AUDIENCE.contains(newCourse.getAudience())) {  // Brezanek
                text = StUtils.renderTemplate("bookingnewconfrm", values);
            } else {  // VIGVAM
                text = StUtils.renderTemplate("bookingnewconfrm2", values);
            }
            mailService.send2(res.getParticipant().getPerson().getUser().getEmail(), "Prodloužená rezervace na další období", text);
        });
    }
    
}
