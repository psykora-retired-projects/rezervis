/*
 * Copyright (c) 2014-2016, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package biz.sykora.rezervis.jsf.backing;

import biz.sykora.rezervis.ejb.CourseService;
import biz.sykora.rezervis.ejb.UserService;
import biz.sykora.rezervis.jsf.util.DateUtils;
import biz.sykora.rezervis.jsf.util.WeekDay;
import biz.sykora.rezervis.model.CourseReservation;
import biz.sykora.rezervis.model.Membership;
import biz.sykora.rezervis.model.Name;
import biz.sykora.rezervis.model.OfferedCourse;
import biz.sykora.rezervis.model.Payment;
import biz.sykora.rezervis.model.Person;
import biz.sykora.rezervis.model.User;
import java.io.IOException;
import java.io.Serializable;
import java.text.Normalizer;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.PrintSetup;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

@Named(value = "exports")
@RequestScoped
public class ExportsView implements Serializable {

    private static final Logger logger = Logger.getLogger(ExportsView.class.getName());

    @Inject
    private UserService userService;

    @Inject
    private CourseService courseService;

    private final String timestampStr = DateUtils.dateTimeIsoCompactFormat().format(new Date());

    private final Map<String, Long> course2id = new LinkedHashMap<>();

    private long selectedCourseId;

    private FacesContext context;
    private ExternalContext externalContext;

    public Map<String, Long> getCourse2id() {
        return course2id;
    }

    public long getSelectedCourseId() {
        return selectedCourseId;
    }

    public void setSelectedCourseId(long selectedCourseId) {
        this.selectedCourseId = selectedCourseId;
    }

    public void downloadActiveUsers() throws IOException {
        logger.log(Level.INFO, "Exporting all the active users.");

        int currYear = Calendar.getInstance().get(Calendar.YEAR);
        int prevYear = currYear - 1;
        int nextYear = currYear + 1;
        List<Integer> membershipYears = Arrays.asList(new Integer[]{prevYear, currYear, nextYear});

        Comparator<Membership> byYear = (m1, m2) -> Integer.compare(m1.getYear(), m2.getYear());
        Comparator<Membership> byHalfYear = (m1, m2) -> Integer.compare(m1.getHalfYear(), m2.getHalfYear());

        List<User> users = userService.getActiveUsers();

        Workbook workbook = new HSSFWorkbook();
        CreationHelper createHelper = workbook.getCreationHelper();
        Sheet sheet = workbook.createSheet("Reg. uživatelé " + timestampStr);
        CellStyle dateStyle = workbook.createCellStyle();
        dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd.mm.yyyy"));

        // create header
        int rowIndex = 0;
        Row rowHdr = sheet.createRow(rowIndex++);
        int columnIndexHdr = 0;
        rowHdr.createCell(columnIndexHdr++).setCellValue("ID");
        rowHdr.createCell(columnIndexHdr++).setCellValue("E-mail");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Jméno");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Ulice");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Město");
        rowHdr.createCell(columnIndexHdr++).setCellValue("PSČ");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Rok nar.");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Telefon");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Tel. 2");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Čl." + prevYear + "/1");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Čl." + prevYear + "/2");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Čl." + currYear + "/1");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Čl." + currYear + "/2");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Čl." + nextYear + "/1");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Čl." + nextYear + "/2");
        int numberOfColumns = columnIndexHdr;

        // create rows
        for (User u : users) {
            Row row = sheet.createRow(rowIndex++);
            int columnIndex = 0;
            row.createCell(columnIndex++).setCellValue(u.getId());
            row.createCell(columnIndex++).setCellValue(u.getEmail());
            Person p = u.getPerson();
            if (p != null) {
                row.createCell(columnIndex++).setCellValue(buildName(p.getName()));
                row.createCell(columnIndex++).setCellValue(p.getAddress().getStreet());
                row.createCell(columnIndex++).setCellValue(p.getAddress().getCity());
                row.createCell(columnIndex++).setCellValue(p.getAddress().getPostcode());
                row.createCell(columnIndex++).setCellValue(p.getYearBorn());
                row.createCell(columnIndex++).setCellValue(p.getPhone());
                row.createCell(columnIndex++).setCellValue(p.getPhone2());
                
                // find membership
                for (int y : membershipYears) {
                    // process the first half year
                    List<Membership> m1 = p.getMembership().stream().filter(m -> m.getYear() == y && m.getHalfYear() == 1).collect(Collectors.toList());
                    if (!m1.isEmpty()) {
                        // TODO: check, if m1.size() == 1
                        Payment pmt = m1.get(0).getPayment();
                        row.createCell(columnIndex++).setCellValue(pmt == null ? "NEPLACENO?" : DateUtils.dateFormat().format(pmt.getPaymentDate()));
                    } else {
                        row.createCell(columnIndex++).setCellValue("NE");
                    }
                    // process the second half year
                    List<Membership> m2 = p.getMembership().stream().filter(m -> m.getYear() == y && m.getHalfYear() == 2).collect(Collectors.toList());
                    if (!m2.isEmpty()) {
                        // TODO: check, if m1.size() == 1
                        Payment pmt = m2.get(0).getPayment();
                        row.createCell(columnIndex++).setCellValue(pmt == null ? "NEPLACENO?" : DateUtils.dateFormat().format(pmt.getPaymentDate()));
                    } else {
                        row.createCell(columnIndex++).setCellValue("NE");
                    }
                }
            }
        }
        for (int i = 0; i < numberOfColumns; i++) {
            sheet.autoSizeColumn(i);
        }

        externalContext.responseReset();
        externalContext.setResponseContentType("application/vnd.ms-excel");
        externalContext.setResponseHeader("Content-Disposition", "attachment;filename=uzivatele-" + timestampStr + ".xls");
        workbook.write(externalContext.getResponseOutputStream());
        context.responseComplete(); // Prevent JSF from performing navigation.    
    }

    public void downloadReservations() throws IOException {
        logger.log(Level.INFO, "Exporting all the current course booking records.");

        List<CourseReservation> bookings = courseService.findCurrentReservations();

        Workbook workbook = new HSSFWorkbook();
        CreationHelper createHelper = workbook.getCreationHelper();
        Sheet sheet = workbook.createSheet("Rezervace k " + timestampStr);
        CellStyle dateStyle = workbook.createCellStyle();
        dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd.mm.yyyy"));
        CellStyle timeStyle = workbook.createCellStyle();
        timeStyle.setDataFormat(createHelper.createDataFormat().getFormat("hh:mm"));
        CellStyle dateTimeStyle = workbook.createCellStyle();
        dateTimeStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd.mm.yyyy hh:mm"));

        // create header
        int rowIndex = 0;
        Row rowHdr = sheet.createRow(rowIndex++);
        int columnIndexHdr = 0;
        rowHdr.createCell(columnIndexHdr++).setCellValue("ID");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Den");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Čas od");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Čas do");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Kurz");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Cena");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Věk");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Účastník");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Dat. nar.");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Čas rezervace");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Dní");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Rodič");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Telefon");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Tel. 2");
        rowHdr.createCell(columnIndexHdr++).setCellValue("E-mail");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Pozice");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Zaplaceno");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Pozn. platby");
        rowHdr.createCell(columnIndexHdr++).setCellValue("ID kr.");
        rowHdr.createCell(columnIndexHdr++).setCellValue("ID účst.");
        rowHdr.createCell(columnIndexHdr++).setCellValue("ID rod.");
        int numberOfColumns = columnIndexHdr;

        // create rows
        Cell cell;
        for (CourseReservation b : bookings) {
            Row row = sheet.createRow(rowIndex++);
            int columnIndex = 0;
            row.createCell(columnIndex++).setCellValue(b.getId());
            row.createCell(columnIndex++).setCellValue(b.getOfferedCourse().getWeekDay());
            cell = row.createCell(columnIndex++);
            cell.setCellValue(b.getOfferedCourse().getMinuteBegin() / 1440.0);
            cell.setCellStyle(timeStyle);
            cell = row.createCell(columnIndex++);
            cell.setCellValue(b.getOfferedCourse().getMinuteEnd() / 1440.0);
            cell.setCellStyle(timeStyle);
            row.createCell(columnIndex++).setCellValue(b.getOfferedCourse().getName());
            row.createCell(columnIndex++).setCellValue(b.getOfferedCourse().getPrice());
            row.createCell(columnIndex++).setCellValue(b.getOfferedCourse().getAge());
            row.createCell(columnIndex++).setCellValue(buildName(b.getParticipant().getName()));
            cell = row.createCell(columnIndex++);
            Date dateBorn = b.getParticipant().getDateBorn();
            if (dateBorn != null) {
                cell.setCellValue(dateBorn);
                cell.setCellStyle(dateStyle);
            }
            cell = row.createCell(columnIndex++);
            cell.setCellValue(b.getReservationTime());
            cell.setCellStyle(dateTimeStyle);
            long diff = new Date().getTime() - b.getReservationTime().getTime();
            long diffDays = TimeUnit.MILLISECONDS.toDays(diff);
            row.createCell(columnIndex++).setCellValue(diffDays);
            row.createCell(columnIndex++).setCellValue(buildName(b.getParticipant().getPerson().getName()));
            row.createCell(columnIndex++).setCellValue(b.getParticipant().getPerson().getPhone());
            row.createCell(columnIndex++).setCellValue(b.getParticipant().getPerson().getPhone2());
            row.createCell(columnIndex++).setCellValue(b.getParticipant().getPerson().getUser().getEmail());
            row.createCell(columnIndex++).setCellValue(b.getRank());
            Payment p = b.getPayment();
            row.createCell(columnIndex++).setCellValue(p == null ? "NE" : DateUtils.dateFormat().format(p.getPaymentDate()));
            row.createCell(columnIndex++).setCellValue(p == null ? "" : p.getComment());
            row.createCell(columnIndex++).setCellValue(b.getOfferedCourse().getId());
            row.createCell(columnIndex++).setCellValue(b.getParticipant().getId());
            row.createCell(columnIndex++).setCellValue(b.getParticipant().getPerson().getId());
        }
        for (int i = 0; i < numberOfColumns; i++) {
            sheet.autoSizeColumn(i);
        }

        externalContext.responseReset();
        externalContext.setResponseContentType("application/vnd.ms-excel");
        externalContext.setResponseHeader("Content-Disposition", "attachment;filename=rezervace-" + timestampStr + ".xls");
        workbook.write(externalContext.getResponseOutputStream());
        context.responseComplete(); // Prevent JSF from performing navigation.    
    }

    public void downloadAttendanceList() throws IOException {
        logger.log(Level.INFO, "Creating list of participants of the course: {0}.", String.valueOf(selectedCourseId));

        OfferedCourse course = courseService.findCourse(selectedCourseId);

        List<CourseReservation> bookings = courseService.findReservationsByCourseName(course.getName());

        Workbook workbook = new HSSFWorkbook();
        CreationHelper createHelper = workbook.getCreationHelper();
        Sheet sheet = workbook.createSheet(course.getName() + " - prezenčka");
        PrintSetup ps = sheet.getPrintSetup();
        ps.setFitWidth((short) 1);
        ps.setPaperSize(PrintSetup.A4_PAPERSIZE);
        ps.setLandscape(true);
        sheet.setAutobreaks(true);
        CellStyle timeStyle = workbook.createCellStyle();
        timeStyle.setDataFormat(createHelper.createDataFormat().getFormat("hh:mm"));
        timeStyle.setBorderBottom(CellStyle.BORDER_THIN);
        timeStyle.setBorderTop(CellStyle.BORDER_THIN);
        timeStyle.setBorderRight(CellStyle.BORDER_THIN);
        timeStyle.setBorderLeft(CellStyle.BORDER_THIN);
        CellStyle borderStyle = workbook.createCellStyle();
        borderStyle.setBorderBottom(CellStyle.BORDER_THIN);
        borderStyle.setBorderTop(CellStyle.BORDER_THIN);
        borderStyle.setBorderRight(CellStyle.BORDER_THIN);
        borderStyle.setBorderLeft(CellStyle.BORDER_THIN);

        // create header
        int rowIndex = 0;
        Row rowHdr = sheet.createRow(rowIndex++);
        rowHdr.createCell(1).setCellValue("Kurz:");
        rowHdr.createCell(2).setCellValue(course.getName());
        rowHdr = sheet.createRow(rowIndex++);
        rowHdr.createCell(1).setCellValue("Lektor:");
        rowHdr.createCell(2).setCellValue(course.getInstructor().getName().getFullName());
        rowIndex++;
        rowHdr = sheet.createRow(rowIndex++);
        int columnIndexHdr = 0;
        rowHdr.createCell(columnIndexHdr++).setCellValue("Den");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Čas od");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Jméno");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Telefon");
        rowHdr.createCell(columnIndexHdr++).setCellValue("Zaplaceno");
        int numberOfColumns = columnIndexHdr;

        // create rows
        Cell cell;
        for (CourseReservation b : bookings) {
            Row row = sheet.createRow(rowIndex++);
            row.setHeightInPoints(Math.round(1.6 * sheet.getDefaultRowHeightInPoints()));
            int columnIndex = 0;
            cell = row.createCell(columnIndex++);
            cell.setCellValue(WeekDay.valueOf(b.getOfferedCourse().getWeekDay()).getAbbrevCap());
            cell.setCellStyle(borderStyle);
            cell = row.createCell(columnIndex++);
            cell.setCellValue(b.getOfferedCourse().getMinuteBegin() / 1440.0);
            cell.setCellStyle(timeStyle);
            cell = row.createCell(columnIndex++);
            cell.setCellValue(b.getParticipant().getName().getFullName());
            cell.setCellStyle(borderStyle);
            cell = row.createCell(columnIndex++);
            cell.setCellValue(b.getParticipant().getPerson().getPhone());
            cell.setCellStyle(borderStyle);
            Payment p = b.getPayment();
            cell = row.createCell(columnIndex++);
            cell.setCellValue(p == null ? " " : DateUtils.dateFormat().format(p.getPaymentDate()));
            cell.setCellStyle(borderStyle);
            for (int i = 0; i < 18; i++) {
                cell = row.createCell(columnIndex++);
                cell.setCellValue(" ");
                cell.setCellStyle(borderStyle);
            }
        }
        for (int i = 0; i < numberOfColumns; i++) {
            sheet.autoSizeColumn(i);
        }
        for (int i = numberOfColumns; i < numberOfColumns + 18; i++) {
            sheet.setColumnWidth(i, sheet.getColumnWidth(i) / 2);
        }

        externalContext.responseReset();
        externalContext.setResponseContentType("application/vnd.ms-excel");
        String fileName = Normalizer.normalize(course.getName(), Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
        externalContext.setResponseHeader("Content-Disposition", "attachment;filename=" + fileName + ".xls");
        workbook.write(externalContext.getResponseOutputStream());
        context.responseComplete(); // Prevent JSF from performing navigation.    
    }

    private String buildName(Name name) {
        StringBuilder bld = new StringBuilder(name.getFamilyName());
        if (name.getFirstName() != null) {
            bld.append(' ');
            bld.append(name.getFirstName());
        }
        return bld.toString();
    }

    @PostConstruct
    public void init() {
        context = FacesContext.getCurrentInstance();
        externalContext = context.getExternalContext();
        // populate dropbox with courses
        courseService.getAllCourses().forEach(this::populateCourse);
    }

    private void populateCourse(OfferedCourse course) {
        String name = course.getName();
        if (course.getInstructor() != null) {
            name = name + " - " + course.getInstructor().getName().getFamilyName();
        }
        if (!course2id.containsKey(name)) {
            course2id.put(name, course.getId());
        }
    }
}
