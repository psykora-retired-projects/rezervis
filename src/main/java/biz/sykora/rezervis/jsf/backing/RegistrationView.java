/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package biz.sykora.rezervis.jsf.backing;

import biz.sykora.rezervis.ejb.MailService;
import biz.sykora.rezervis.ejb.UserService;
import biz.sykora.rezervis.cdi.IdentityManager;
import biz.sykora.rezervis.jsf.util.StUtils;
import biz.sykora.rezervis.model.User;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Null;

@Named(value = "registration")
@RequestScoped
public class RegistrationView {
    private static final Logger logger = Logger.getLogger(RegistrationView.class.getName());
    private static final Pattern emailPattern = Pattern.compile("([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)"); 

    @Inject
    private UserService userService; 
    
    @Inject
    private MailService mailService;
    
    @Inject
    private IdentityManager identityManager;
    
    private String email;
    private String password;
    private String password2;
    
    @Null(message = "Toto pole musí zůstat prázdné!")
    private String honeypot;
    
    public RegistrationView() {
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    public String getHoneypot() {
        return honeypot;
    }

    public void setHoneypot(String honeypot) {
        this.honeypot = honeypot;
    }
    
    public String register() {
        identityManager.logout();
        this.userService.saveNewUser(email, password);
        sendConfirmationEmail();
        email = null;
        password = null;
        password2 = null;
        return "registration-ok";
    }
    
    public void validateEmail(FacesContext context, UIComponent component, Object value) {
        String myEmail = value.toString();
        Matcher m = emailPattern.matcher(myEmail);
        if (!m.matches()) {
            ((UIInput)component).setValid(false);
            FacesMessage msg = new FacesMessage("Nesprávný formát e-mailové adresy!");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            context.addMessage(component.getClientId(context), msg);
        } else if (this.userService.findUserExist(myEmail)) {
            ((UIInput)component).setValid(false);
            FacesMessage msg = new FacesMessage("Tato e-mailová adresa je již registrována!");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            context.addMessage(component.getClientId(context), msg);
        }
    }
    
    public void sendConfirmationEmail() {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest)fc.getExternalContext().getRequest();
        User u = userService.find(email);
        logger.log(Level.INFO, "Sending confirmation e-mail for {0}.", email);
        if (u.getVerificationKey() == null) {
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Tato emailová adresa je již ověřená!", null));
        }
        try {
            String path = req.getContextPath() + "/confirm-email.xhtml";
            URI uri = new URI(req.getScheme(), null, req.getServerName(), req.getServerName().equals("localhost") ? req.getServerPort() : -1, path, "key="+u.getVerificationKey(), null);
            Map<String, Object> values = new HashMap<>();
            values.put("link", uri.toASCIIString());
            String text = StUtils.renderTemplate("registconfrm", values);
            mailService.send2(email, "Vaše registrace do rezervačního systému", text);
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Zaslán ověřovací e-mail na adresu " + email, null));
        } catch (URISyntaxException | RuntimeException ex) {
            logger.log(Level.SEVERE, null, ex);
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Chyba při zasílání ověřovacího e-mailu! Kontaktujte prosím recepci.", null));
        }
            
    } 
    
}
