/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package biz.sykora.rezervis.jsf.backing;

import biz.sykora.rezervis.cdi.IdentityManager;
import biz.sykora.rezervis.ejb.UserService;
import biz.sykora.rezervis.model.Gender;
import biz.sykora.rezervis.model.Name;
import biz.sykora.rezervis.model.Participant;
import biz.sykora.rezervis.model.Person;
import biz.sykora.rezervis.model.User;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

@Named(value = "children")
@ViewScoped
public class ChildrenView implements Serializable {
    private static final Logger logger = Logger.getLogger(ChildrenView.class.getName());

    @Inject
    private UserService userService; 
    
    @Inject
    private IdentityManager identityManager;

    //private long userId;
    
    private Person person;
    
    public ChildrenView() {}

    private List<ChildData> children = new LinkedList<>();

    public Person getPerson() {
        return person;
    }
    
    public List<ChildData> getChildren() {
        return children;
    }
    
    // creates or updates a particular child
    public String save(ChildData childData) {
        Name name = new Name(childData.getFirstName(), childData.getFamilyName());
        Gender gender = Gender.valueOf(childData.getGenderStr());
        if (childData.getId() == 0) {
            long id = userService.addChild(person, name, gender, childData.getDateBorn());
            childData.setId(id);
            logger.log(Level.INFO, "Child added: {0}", childData);
        } else {
            userService.updateChild(childData.getId(), name, gender, childData.getDateBorn());
            logger.log(Level.INFO, "Child updated: {0}", childData);
        }
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Údaje dítěte \"" + name.getFamilyName() + ", " + name.getFirstName() +  "\" byly uloženy do databáze.", null));
        return null;
    }
    
    public String add() {
        children.add(0, new ChildData(0, null, null, "FEMALE", null));
        return null;
    }
        
    @PostConstruct
    public void init() {
        long userId= identityManager.getUser().getId();
        User u = userService.find(userId);
        person = u.getPerson();
        if (person == null) {
            return;
        }
        for (Participant p : person.getParticipants()) {
            if (p.isSelf()) {
                continue;
            }
            ChildData child = new ChildData(
                    p.getId(),
                    p.getName().getFirstName(),
                    p.getName().getFamilyName(),
                    p.getGender().name(),
                    p.getDateBorn()
            );
            children.add(child);
        }
    }
 
    public static class ChildData implements Serializable {

        private long id;
        
        private String genderStr;

        @NotNull(message = "Jméno musí být zadáno!")
        @Size(min = 2, max = 32, message = "Délka jména musí být 2 až 32 znaků.")
        private String firstName;

        @NotNull(message = "Příjmení musí být zadáno!")
        @Size(min = 2, max = 32, message = "Délka příjmení musí být 2 až 32 znaků.")
        private String familyName;

        @NotNull(message = "Datum narození musí být zadáno!")
        @Past(message = "Datum narození nesmí být v budoucnosti.")
        Date dateBorn;

        public ChildData() {}

        public ChildData(long id, String firstName, String familyName, String genderStr, Date dateBorn) {
            this.id = id;
            this.firstName = firstName;
            this.familyName = familyName;
            this.genderStr = genderStr;
            this.dateBorn = dateBorn;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }
        
        public String getGenderStr() {
            return genderStr;
        }

        public void setGenderStr(String genderStr) {
            this.genderStr = genderStr;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getFamilyName() {
            return familyName;
        }

        public void setFamilyName(String lastName) {
            this.familyName = lastName;
        }

        public Date getDateBorn() {
            return dateBorn;
        }

        public void setDateBorn(Date dateBorn) {
            this.dateBorn = dateBorn;
        }

        @Override
        public String toString() {
            return "ChildData{" + "id=" + id + ", genderStr=" + genderStr + ", firstName=" + firstName + ", familyName=" + familyName + ", dateBorn=" + dateBorn + '}';
        }

    }
}
