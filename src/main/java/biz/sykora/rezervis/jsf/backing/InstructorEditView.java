/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package biz.sykora.rezervis.jsf.backing;

import biz.sykora.rezervis.ejb.CourseService;
import biz.sykora.rezervis.model.Instructor;
import biz.sykora.rezervis.model.Name;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;

@Named(value = "instrEdit")
@ViewScoped
public class InstructorEditView implements Serializable {

    private static final Logger logger = Logger.getLogger(InstructorEditView.class.getName());

    @Inject
    CourseService courseService;

    private long instrId;  // ID of the instructor to edit (comes via query parameters), 0 for a new instructor creation

    private String firstName;
    
    @NotNull(message="Příjmení musí být zadáno!")
    private String famillyName;
    
    private String description;
    
    private String urlExt;
        
    public long getInstrId() {
        return instrId;
    }

    public void setInstrId(long instrId) {
        this.instrId = instrId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFamillyName() {
        return famillyName;
    }

    public void setFamillyName(String famillyName) {
        this.famillyName = famillyName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrlExt() {
        return urlExt;
    }

    public void setUrlExt(String urlExt) {
        this.urlExt = urlExt;
    }

    public String submit() {
        Name name = new Name(firstName, famillyName);
        if (instrId == 0) {
            // creating new instructor
            Instructor instr = new Instructor();
            instr.setName(name);
            instr.setDescription(description);
            instr.setUrlExt(urlExt);
            courseService.createInstructor(instr);
            logger.log(Level.INFO, "Created {0}", instr.toString());
        } else {
            // updating existing instructor
            Instructor instr = courseService.findInstructor(instrId);
            instr.setName(name);
            instr.setDescription(description);
            instr.setUrlExt(urlExt);
            courseService.updateInstructor(instr);
            logger.log(Level.INFO, "Updated {0}", instr.toString());
        }
        
        return "instr-mgmt?faces-redirect=true";
    }
    
    public void init() {
        if (instrId != 0) {
            Instructor instr = courseService.findInstructor(instrId);
            firstName = instr.getName().getFirstName();
            famillyName = instr.getName().getFamilyName();
            description = instr.getDescription();
            urlExt = instr.getUrlExt();
        }
    }

}
