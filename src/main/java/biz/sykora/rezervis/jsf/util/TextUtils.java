/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package biz.sykora.rezervis.jsf.util;

import java.util.regex.Pattern;

public class TextUtils {
    private static final Pattern LINK_PATTERN = Pattern.compile("(https?\\://[\\w-\\.]+\\.[a-z]{2,6}(/\\S*)?)", Pattern.CASE_INSENSITIVE);
    private static final Pattern EMAIL_PATTERN = Pattern.compile("([\\:]?)([\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,6})");  // TLD with max 6 chars

    // removes <, >, & and finds URLs and e-mails
    public static String sanitizeHtml(String text) {
        // remove <, > and &
        String res = text.replace("<", "").replace(">", "").replace("&", "");
        // find e-mail adresses, make links and surround them with the HTML <a>
        res = EMAIL_PATTERN.matcher(res).replaceAll("$1<a href='mailto:$2'>$2</a>");
        // find link candidates, make proper links and surround them with the HTML <a>
        res = LINK_PATTERN.matcher(res).replaceAll("<a href='$1'>$1</a>");
        return res;
    }
    
}
