/*
 * Copyright (c) 2014-2015, Pavel Sykora
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package biz.sykora.rezervis.jsf.util;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * A container for a week day names and ordinal numbers
 */
public enum WeekDay {

    MON(1, "pondělí", "Po"),
    TUE(2, "úterý", "Út"),
    WED(3, "středa", "St"),
    THU(4, "čtvrtek", "Čt"),
    FRI(5, "pátek", "Pá"),
    SAT(6, "sobota", "So"),
    SUN(7, "neděle", "Ne");

    private static final Map<Integer, WeekDay> lookup = new HashMap<>();

    static {
        EnumSet.allOf(WeekDay.class).forEach( (day) -> lookup.put(day.getNumber(), day) );
    }

    public static List<WeekDay> getWeekDays() {
        return Arrays.asList(MON, TUE, WED, THU, FRI, SAT, SUN);
    }
    
    public static WeekDay valueOf(int ordinal) {
        return lookup.get(ordinal);
    }

    private final int number;
    private final String name;
    private final String abbrevCap;
    private final String nameCap;

    private WeekDay(int number, String name, String abbrevCap) {
        this.number = number;
        this.name = name;
        this.abbrevCap = abbrevCap;
        this.nameCap = abbrevCap + name.substring(2);
    }

    /**
     * @return week day ordinal number (1..7) starting with Monday
     */
    public int getNumber() {
        return number;
    }

    /**
     * @return week day name
     */
    public String getName() {
        return name;
    }

    /**
     * @return week day capitalized abbreviation
     */
    public String getAbbrevCap() {
        return abbrevCap;
    }

    /**
     * @return week day capitalized name
     */
    public String getNameCap() {
        return nameCap;
    }

}
