# instructors and offered courses: JSON to SQL inserts

# pro krouzky v tabulce Google Apps: 
# * data a casy musí byt formatovany jako text
# * po exportu se musi odstranit posledni zaznam - je tam chybne jen polozka audience

import json

lektori = json.load(open("lektori.json", "r", encoding="utf-8"))

krouzky = json.load(open("krouzky.json", "r", encoding="utf-8"))

sqlPreambule = """
DELETE FROM COURSE_REGISTRATIONS;
DELETE FROM OFFERED_COURSES;
DELETE FROM INSTRUCTORS;
ALTER TABLE OFFERED_COURSES DROP CONSTRAINT IF EXISTS UQ_OFFERED_COURSES_NAME;

"""

def value_by_key_prefix(d, partial):
    matches = [val for key, val in d.items() if partial in key]
    if not matches:
        return 0
    if len(matches) > 1:
        raise ValueError('{} matches more than one key'.format(partial))
    return matches[0]

id = 100;

lektor2id = {}

with open("lektori-krouzky.sql", "w", encoding="utf-8") as file:
    file.write(sqlPreambule);
    for lektor in lektori :
        print(lektor)
        lektor2id[lektor.get('name')] = id
        file.write("INSERT INTO INSTRUCTORS (ID, ACTIVE, FAMILY_NAME, FIRST_NAME, ID_EXT, URL_EXT, CREATED_BY) VALUES (")
        file.write(str(id))
        id += 1
        file.write(", TRUE, '")
        if 'LEKTOR' in lektor.get('name').upper() :
            file.write(lektor.get('name'))
            file.write("', NULL, ")
        else :
            name = lektor.get('name').split()
            file.write(name[0])
            file.write("', '")
            file.write(name[1])
            file.write("', ")
        file.write(str(lektor.get("id_ext")))
        file.write(", '")
        file.write(lektor.get("url_ext"))
        file.write("', '")
        file.write("IMPORT")
        file.write("');\n")

    for krouzek in krouzky:
        print(krouzek.get('coursename'), krouzek.get('lecturername'), value_by_key_prefix(lektor2id, krouzek.get('lecturername')))
        file.write("INSERT INTO OFFERED_COURSES (ID, ACTIVE, SEASON_ID, WEEK_DAY, MINUTE_BEGIN, MINUTE_END, INSTRUCTOR_ID, NAME, AUDIENCE, AGE, DATE_BEGIN, NUMBER_OF_LESSONS, PRICE, CAPACITY, BALANCE, COMMENT, URL_EXT, CREATED_BY) VALUES (")
        file.write(str(krouzek.get('id')))
        file.write(", ")   
        file.write('TRUE')    
        file.write(", ")   
        file.write(str(krouzek.get('seasonid')))    
        file.write(", ")   
        file.write(str(krouzek.get('weekday')))
        file.write(", ")   
        minutes = krouzek.get('timebegin').split(':')    
        file.write(str(int(minutes[0])*60+int(minutes[1])))    
        file.write(", ")   
        minutes = krouzek.get('timeend').split(':')    
        file.write(str(int(minutes[0])*60+int(minutes[1])))
        file.write(", ")   
        file.write(str(value_by_key_prefix(lektor2id, krouzek.get('lecturername'))))    
        file.write(", '")   
        file.write(krouzek.get('coursename'))    
        file.write("', '")   
        file.write(krouzek.get('audience'))    
        file.write("', ")   
        age = krouzek.get('age')
        if age is None :   
            file.write('NULL, ')
        else :
            file.write("'")
            file.write(age)    
            file.write("', ")
        datebegin = krouzek.get('datebegin')
        if datebegin is None :   
            file.write('NULL, ')
        else :
            file.write("DATE '")
            date = datebegin.split('.')
            file.write(date[2] + '-' + date[1] + '-' + date[0])    
            file.write("', ")
        file.write(str(krouzek.get('lessons')))
        file.write(", ")   
        file.write(str(krouzek.get('price')))    
        file.write(", ")   
        file.write(str(krouzek.get('capacity')))    
        file.write(", ")   
        file.write(str(krouzek.get('capacity')))    
        file.write(", ")   
        comment = krouzek.get('comment')
        if comment is None :   
            file.write('NULL, ')
        else :
            file.write("'")
            file.write(comment)    
            file.write("', ")
        urlext = krouzek.get('urlext')
        if urlext is None :   
            file.write('NULL, ')
        else :
            file.write("'")
            file.write(urlext)    
            file.write("', ")
        file.write("'IMPORT');\n")    
