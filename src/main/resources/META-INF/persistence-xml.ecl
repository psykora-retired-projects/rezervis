<?xml version="1.0" encoding="UTF-8"?>
<persistence version="2.1" xmlns="http://xmlns.jcp.org/xml/ns/persistence" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/persistence http://xmlns.jcp.org/xml/ns/persistence/persistence_2_1.xsd">
  <persistence-unit name="rezervisdb" transaction-type="JTA">
    <provider>org.eclipse.persistence.jpa.PersistenceProvider</provider>
    <exclude-unlisted-classes>false</exclude-unlisted-classes>
    <properties>
    	<property name="eclipselink.target-database" value="org.eclipse.persistence.platform.database.H2Platform"/>
    	<property name="javax.persistence.jdbc.user" value="sa"/> <!--  use real username here --> 
    	<property name="javax.persistence.jdbc.password" value="sa"/> <!--  use real password here -->
    	<property name="javax.persistence.jdbc.driver" value="org.h2.Driver"/>
    	<property name="javax.persistence.jdbc.url" value="jdbc:h2:~/NetBeansBrezanek/db/rezervace;AUTO_SERVER=TRUE"/> <!--  use real JDBC URL here -->
    	 <property name="eclipselink.logging.level" value="FINE"/>
    	<property name="eclipselink.logging.level.sql" value="FINE"/>
    	<property name="eclipselink.logging.parameters" value="true"/>
    </properties>
  </persistence-unit>
</persistence>
