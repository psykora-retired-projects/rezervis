A Docker image for Rezervis web application - development version.

Notes:

* Copy "rezervis.war" into this directory before build.
* This image must not be published in the Docker Hub.
* This image should be (re)built for each new version of "rezervis.war".
* A container needs a h2db container and network - see docker compose.

Build:
	`docker build -t pavsyk/rezervis-dev .`

Run via docker compose.
