A Docker image based on CentOS 7 with headless JRE 8, EPEL repo etc. The purpose of the image 
is to be a base for other images which need Java.

This image is inspired by airhacks/java image by Adam Bien.

Build:
	`docker build -t pavsyk/java .`